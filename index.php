<!DOCTYPE HTML>
<html>


<meta property="fb:app_id"						content="166672083743530"/>
<meta property="og:type"               content="article" />
<meta property="og:title"              content="Cariin dong game" />
<?php
if(isset($_GET["level"])){
	echo '<meta property="og:url"                content="https://www.tandafuntastrip.com/games/cariin_dong_staging/index.php?level='.$_GET["level"].'" />';
	$currentLevels = 0;
	if($_GET["level"]%15 == 0)
	{
		$currentLevels = 15;
	}else{
		$currentLevels = $_GET["level"]%15;
	}
	echo '<meta property="og:description"        content="Ayo bermain games cariin dong, aku sudah selesai level '.$currentLevels.'"/>';
	echo '<meta property="og:image"              content="https://www.tandafuntastrip.com/games/cariin_dong_staging/Share/'.$_GET["level"].'.jpg"/>';
}else{
	echo '<meta property="og:url"                content="https://www.tandafuntastrip.com/games/cariin_dong_staging/index.php" />';
	echo '<meta property="og:description"        content="Program Tanda Funtastrip 2016 adalah program unggulan dari Bank OCBC NISP yang memberikan kesempatan bagi semua orang - nasabah, maupun non nasabah -untuk mendapatkan berbagai hadiah menarik seperti voucher belanja, gadget, merchandise, voucher ke Bali, Lombok, Hong Kong, Tokyo, dan London" />';
	echo '<meta property="og:image"              content="https://www.tandafuntastrip.com/games/cariin_dong_staging/asset/SplashScreen/background splash.jpg"/>';
}

?>
<meta property="og:image:width" content="1920" />
<meta property="og:image:height" content="1080" />
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui">
	<link rel="manifest" href="manifest.json">
	<title>OCBC - Cariin Dong!</title>
	<meta name="keyword" content="OCBC NISP, Tanda Funtastrip, Tandafuntastrip, Liburan gratis, liburan rp 0, Liburan gratis Bali, Liburan gratis Lombok, Liburan gratis Tokyo, Liburan gratis Hong Kong,  Liburan gratis London" />
	<!-- <meta name="author" content="pixelnine"/> -->
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/sly.css" type="text/css" />
	<link rel="icon" type="image/png" href="asset/favicon.png" />
	<script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Sly/1.6.1/sly.min.js"></script>
	<script type="text/javascript" src="vendor/plugin.js"></script>
	<script src="bower_components/phaser/build/phaser.min.js"></script>
	<meta name="mobile-web-app-capable" content="yes">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



	<!-- <script src="src/min/src.min.js"></script> -->

	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-81712035-1', 'auto');
	ga('send', 'pageview');

	</script>
</head>

<body style="background-image: url('https://tandafuntastrip.com/application/views/front/fullwidth/images/large/login.jpg')">
	<div id="game-canvas">
		<div class="wrap" id="hint-container" >
			<div class="wrap-1">
				<img src="asset/exit btn.png" id="close-button-info" class="button-close">
				<div class="wrap-2 paragraph-info">
					Aturan Bermain Game Cariin Dong!
					<ul>
						<li>
							Cari objek sesuai daftar pada setiap level sebelum waktu habis.
						</li>
						<li>Setiap kali kamu meng-klik objek yang salah, akan ada penalti pengurangan waktu sebanyak 2 (dua) detik.</li>
						<li>Apabila kamu berhasil menemukan semua benda dalam daftar sebelum waktu habis, maka kamu akan memenangkan level tersebut dan bisa melanjutkan ke level berikutnya. </li>
						<li>Setiap kamu memenangkan level baru, kamu akan mendapatkan coin.</li>
						<li>Pengulangan level yang sudah dilewati tidak menambahkan jumlah coin yang kamu dapat.</li>
					</ul>
					<br/>
					Hint
					<br/>
					<ul>
						<li>Kamu bisa menggunakan 3 (tiga) bantuan dalam tiap level.</li>
						<li>Bantuan pertama bisa dipakai secara cuma-cuma, bantuan ke-dua dan ke-tiga masing-masing akan dikurangi 5 (lima) coin bila dipakai.</li>
					</ul>
					Bonus Coin
					Selain bermain Games, kamu juga bisa mendapatkan tambahan coin dengan berbagai cara lain loh! Info lengkapnya bisa di-cek di <a href="http://www.tandafuntastrip.com/coin">www.tandafuntastrip.com/coin</a>

				</div>
			</div>
		</div>


		<div class="wrap" id="addcoin-container" >
			<div class="wrap-1">
				<div id="close-button-coin" class="button-close"> </div>
				<div class="wrap-2 paragraph-info">
					<div class="frame" id="addcoin-content">
						<div class="clearfix">
							<div class="coin-image-container">
								<image src="asset/add_coin/buka tabungan tanda 360 off.png" class="coin-image" id="btn-360"/>
							</div>
							<div class="coin-image-container">
								<image src="asset/add_coin/buka taka off.png" class="coin-image" id="btn-taka"/>
							</div>
							<div class="coin-image-container">
								<image src="asset/add_coin/bonus tanda poin 1 off.png" class="coin-image" id="btn-bonus-1"/>
							</div>
							<div class="coin-image-container">
								<image src="asset/add_coin/bonus tanda poin 2 off.png" class="coin-image" id="btn-bonus-2"/>
							</div>
							<div class="coin-image-container">
								<image src="asset/add_coin/bonus tanda poin 3.png" class="coin-image" id="btn-bonus-3"/>
							</div>
							<div class="coin-image-container">
								<image src="asset/add_coin/lelang tanda poin 1 off.png" class="coin-image" id="btn-lelang-1"/>
							</div>
							<div class="coin-image-container">
								<image src="asset/add_coin/lelang tanda poin 2 off.png" class="coin-image" id="btn-lelang-2"/>
							</div>
							<div class="coin-image-container">
								<image src="asset/add_coin/lelang tanda poin 3.png" class="coin-image" id="btn-lelang-3"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="black-background">
	<div class="center-container">


		<div id="pernah-error" class="center-content">
			Anda sudah pernah klaim bonus koin ini
		</div>

		<div id="info-message" class="center-content info-container">
			<div id="loading-content" class="center-content">
				Loading.....
			</div>

			<div id="invis-close-btn"></div>
			<div id="info-group">
				<div id="info-content-text">
					error mas bro <br> asfahf kasfh kja hfkjahs f
				</div>
				<img class="btn-info-content" id="btn-kirim" src="asset/btn kirim.png">
				<img class="btn-info-content" id="btn-ok" src="asset/btn ok.png">
			</div>
			<div id="form-tanda-360">
				<label id="label-form-input">Masukkan nomor ATM Anda</label><br/>
					<input type="text" onkeydown="return ( event.ctrlKey || event.altKey
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false)
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9)
                    || (event.keyCode>34 && event.keyCode<40)
                    || (event.keyCode==46) )" id="input-no-atm" maxlength="16"/>
					<img class="btn-info-content form-btn" id="done-atm" src="asset/btn kirim.png">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div id="black-overlay">
</div> -->
<script src="config/config.js"></script>
<script src="src/min/src.min.js?v=2"></script>
<script src="src/dist/coin.js">
</script>
</body>
</html>
