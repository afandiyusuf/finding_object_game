var levelSetting = [];

var level1 = {
  "levelTitle": "level 1 kamar tidur",
  "levelName": "level 1 kamar tidur",
  "hint": "gak ada hint",
  "story":"Oci memerlukan bantuan mencari barang-barang\nyang ia akan bawa traveling ke Bali.",
  "folder_dest": "Level1 Kamar",
  "objectToFind": 10,
  "time_game":120, // in second
  "win_coin":50, // coin yang didapat jika menang
  "totalObject":10,
  "objectData": level1Kamar
};

var level2 = {
  "levelTitle": "level 2 mobil",
  "levelName": "level 2 mobil",
  "hint": "gak ada hint",
  "story":"Saat di perjalanan ke bandara,\nOci mencari barang-barang kecil lain yang ia perlukan untuk traveling.",
  "folder_dest": "Level2 Mobil",
  "objectToFind": 10,
  "time_game":120, // in second
  "win_coin":50, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level2Mobil
};

var level3 = {
  "levelTitle": "level 3 airport",
  "levelName": "level 3 airport",
  "hint": "gak ada hint",
  "story":"Karena terburu-buru mau check-in di bandara, Oci terpeleset dan jatuh,\nbarang-barang bawaan dari tasnya tumpah semua. Bantu ia mencari barang-barang yang penting!",
  "folder_dest": "Level3 Bandara",
  "objectToFind": 10,
  "time_game":120, // in second
  "win_coin":50, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level3Bandara
};

var level4 = {
  "levelTitle": "level 4 cafe",
  "levelName": "level 4 cafe",
  "hint": "gak ada hint",
  "story":"Setelah check-in, Oci duduk di cafe sambil menunggu waktu boarding.\nBantu dia memilih makanan dan minuman, serta merapikan kembali isi tasnya!",
  "folder_dest": "Level4 Cafe",
  "objectToFind": 10,
  "time_game":120, // in second
  "win_coin":50, // coin yang didapat jika menang
  "totalObject":10,
  "objectData": level4Cafe
};

var level5 = {
  "levelTitle": "level 5 passport",
  "levelName": "level 5 passport",
  "hint": "gak ada hint",
  "story":"Setelah mendarat di Bali, saatnya Oci melewati imigrasi di Bali dan mengecap paspornya.\n Carilah berbagai cap berikut di halaman paspor Oci!",
  "folder_dest": "Level5 Passport",
  "objectToFind": 10,
  "time_game":120, // in second
  "win_coin":50, // coin yang didapat jika menang
  "totalObject":10,
  "objectData": level5Passport
};

var level6 = {
  "levelTitle": "level 6 hotel",
  "levelName": "level 6 hotel",
  "hint": "gak ada hint",
  "story" :"Setelah tiba dan check-in di hotel,Oci segera bersiap-siap untuk berjemur di pantai.\nBantu dia menyiapkan hal-hal untuk dibawa ke pantai!",
  "folder_dest": "Level6Hotel1",
  "objectToFind": 10,
  "time_game":90, // in second
  "win_coin":100, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level6Hotel1
};

var level7 = {
  "levelTitle": "level 7 pantai",
  "levelName": "level 7 pantai",
  "hint": "gak ada hint",
  "story":"Oci ingin berjemur di pantai dan melakukan berbagai aktivitas seru di Pantai Kuta!\nKira-kira benda apa saja yang ia butuhkan di pantai?",
  "folder_dest": "Level7 Pantai",
  "objectToFind": 10,
  "time_game":90, // in second
  "win_coin":100, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level7Pantai
};

var level8= {
  "levelTitle": "level 8 cafe",
  "levelName": "level 8 cafe",
  "hint": "gak ada hint",
  "story":"Setelah melihat matahari terbenam, sekarang Oci ingin bersantai di bar pantai.\nTemukan benda-benda ini di bar tersebut!",
  "folder_dest": "Level8 Cafe2",
  "objectToFind": 10,
  "time_game":90, // in second
  "win_coin":100, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level8Cafe
};

var level9= {
  "levelTitle": "level 9 restoran",
  "levelName": "level 9 restoran",
  "hint": "gak ada hint",
  "story":"Oci kelaparan dan memutuskan untuk mencari makan di restoran.\n Temukan benda-benda ini di restoran tempat ia singgah!",
  "folder_dest": "Level9 Restoran",
  "objectToFind": 10,
  "time_game":90, // in second
  "win_coin":100, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level9Restoran
};
var level10= {
  "levelTitle": "level 10 oleh-oleh makanan dan souvenir",
  "levelName": "level 10 oleh-oleh makanan dan souvenir",
  "hint": "gak ada hint",
  "story":"Oci ingin membeli berbagai makanan dan suvenir khas Bali untuk diri dan teman-temannya.\nBantu ia mencari benda-benda ini!",
  "folder_dest": "Level10 OlehOleh",
  "objectToFind": 10,
  "time_game":90, // in second
  "win_coin":100, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level10OlehOleh
};


var level11= {
  "levelTitle": "level 11 pantai",
  "levelName": "level 11 pantai",
  "hint": "gak ada hint",
  "story":"Kali ini Oci ingin ke pantai Sanur! Bantu dia mencari hal-hal ini di pantai Sanur!",
  "folder_dest": "Level11Pantai2",
  "objectToFind": 10,
  "time_game":60, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level11Pantai2
};

var level12= {
  "levelTitle": "level 12 hotel",
  "levelName": "level 12 hotel",
  "hint": "gak ada hint",
  "story":"Sudah saatnya Oci mengepak kembali barang-barangnya sebelum pulang!\nBantu ia mengumpulkan barangnya yang berceceran di kamar hotel!",
  "folder_dest": "Level12Hotel2",
  "objectToFind": 10,
  "time_game":60, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level12Hotel2
};

var level13= {
  "levelTitle": "level 13 cabin pesawat",
  "levelName": "level 13 cabin pesawat",
  "hint": "gak ada hint",
  "story":"Selama perjalanan di pesawat, Oci mengeluarkan berbagai barang.\nSekarang sudah landing dan ia harus mengumpulkannya lagi! Bantu ia agar tak ada barang yang tertinggal!",
  "folder_dest": "Level13Cabin",
  "objectToFind": 10,
  "time_game":60, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level13Cabin
};

var level14= {
  "levelTitle": "level 14 airport",
  "levelName": "level 14 airport",
  "hint": "gak ada hint",
  "story":"Koper Oci terbuka saat pengambilan bagasi, dan isinya berceceran kemana-mana!\nBantu dia mengumpulkan kembali barang-barangnya!",
  "folder_dest": "Level14Airpot",
  "objectToFind": 10,
  "time_game":60, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level14Airpot
};

var level15 = {
  "levelTitle": "level 15 Koper",
  "levelName": "level 15 Koper",
  "hint": "gak ada hint",
  "story":"Setelah kembali ke kamarnya, kini saatnya Oci unpacking!\nBantu dia mengeluarkan berbagai oleh-oleh dari kopernya!",
  "folder_dest": "Level15 Koper",
  "objectToFind": 10,
  "time_game":60, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level15Koper
};

var level16 = {
  "levelTitle": "level 1 Meja",
  "levelName": "level 1 Meja",
  "hint": "gak ada hint",
  "story":"Kali ini Oci akan melakukan perjalanan ke Lombok! \nIa perlu bantuan kamu lagi untuk packing barang-barangnya. \nBantu ia mencari barang-barang di mejanya!",
  "folder_dest": "Level16Meja",
  "objectToFind": 10,
  "time_game":120, // in second
  "win_coin":50, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level16Meja
};

var level17 = {
  "levelTitle": "level 2 Mobil",
  "levelName": "level 2 Mobil",
  "hint": "gak ada hint",
  "story":"Bisakah kamu membantu Oci mencari barang-barang lain \n yang ia perlu bawa yang masih tertinggal di mobilnya?",
  "folder_dest": "Level17Mobil",
  "objectToFind": 10,
  "time_game":120, // in second
  "win_coin":50, // coin yang didapat jika menang
  "totalObject":19,
  "objectData": level17Mobil
};

var level18 = {
  "levelTitle": "level 3 Cabin Ex Class",
  "levelName": "level 3 Cabin Ex Class",
  "hint": "gak ada hint",
  "story":"Sebelum turun dari pesawat, \n Oci harus memeriksa kembali barang bawaannya agar tidak ada yang tertinggal.\nBantu ia mencari barang-barangnya!",
  "folder_dest": "Level18CabinExClass",
  "objectToFind": 10,
  "time_game":120, // in second
  "win_coin":50, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level18CabinExClass
};

var level19 = {
  "levelTitle": "level 4 Pantai",
  "levelName": "level 4 Pantai",
  "hint": "gak ada hint",
  "story":"Oci sedang berjemur di pantai sambil menikmati pemandangan pantai yang indah.\nBisakah kamu menemukan hal-hal yang ia lihat sembari berjemur di pantai ini?",
  "folder_dest": "Level19Pantai",
  "objectToFind": 10,
  "time_game":120, // in second
  "win_coin":50, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level19Pantai
};

var level20 = {
  "levelTitle": "level 5 Diving Equipment",
  "levelName": "level 5 Diving Equipment",
  "hint": "gak ada hint",
  "story":"Oci sudah siap menyelam! Tapi harus dipastikan jangan ada yang tertinggal sebelum dia naik kapal ke tengah laut.\n Bantu dia menyiapkan perlengkapan untuk dibawa ke tengah laut!",
  "folder_dest": "Level20DivingEquipment",
  "objectToFind": 10,
  "time_game":120, // in second
  "win_coin":50, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level20DivingEquipment
};

var level21 = {
  "levelTitle": "level 6 Kapal",
  "levelName": "level 6 Kapal",
  "hint": "gak ada hint",
  "story":"Sekarang Oci berada di kapal menuju tengah laut untuk menyelam.\n Namun kapal terombang-ambing oleh ombak dan barang-barang Oci berceceran di atas dek kapal!\nBantu ia menemukan barang-barangnya!",
  "folder_dest": "Level21Kapal",
  "objectToFind": 10,
  "time_game":90, // in second
  "win_coin":100, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level21Kapal
};

var level22 = {
  "levelTitle": "level 7 Bawah Laut",
  "levelName": "level 7 Bawah Laut",
  "hint": "gak ada hint",
  "story":"Setelah menikmati pemandangan pantai,\nsekarang Oci ingin menikmati pemandangan bawah laut!\nIa melihat banyak sekali keindahan bawah laut. Bisakah kamu menemukannya juga?",
  "folder_dest": "Level22Bawahlaut",
  "objectToFind": 10,
  "time_game":90, // in second
  "win_coin":100, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level22Bawahlaut
};

var level23 = {
  "levelTitle": "level 8 Pasar Buah",
  "levelName": "level 8 Pasar Buah",
  "hint": "gak ada hint",
  "story":"Setelah menyelam ke dasar laut,\n kini Oci merasa lapar dan ingin membeli buah-buahan.\nBisakah kamu menemukan buah-buahan ini?",
  "folder_dest": "Level23PasarBuah",
  "objectToFind": 10,
  "time_game":90, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level23PasarBuah
};

var level24 = {
  "levelTitle": "level 9 Rinjani",
  "levelName": "level 9 Rinjani",
  "hint": "gak ada hint",
  "story":"Oci sedang hiking ke Gunung Rinjani. Di sana ia berkemah,\nnamun sekarang saatnya melanjutkan perjalanan dan membereskan kemahnya.\nBantu Oci untuk beres-beres!",
  "folder_dest": "Level24Rinjani",
  "objectToFind": 10,
  "time_game":90, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level24Rinjani
};

var level25 = {
  "levelTitle": "level 10 Air Terjun",
  "levelName": "level 10 Air Terjun",
  "hint": "gak ada hint",
  "story":"Saat perjalanan kembali ke villa, Oci menemukan sebuah air terjun yang sangat indah.\nDia memutuskan untuk berenang di air terjun tersebut, namun sekarang sudah waktunya pulang!\nBantu Oci mengumpulkan barang-barangnya agar tidak ada yang tertinggal.",
  "folder_dest": "Level25AirTerjun",
  "objectToFind": 10,
  "time_game":90, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level25AirTerjun
};

var level26 = {
  "levelTitle": "level 11 Toko Suvenir",
  "levelName": "level 11 Toko Suvenir",
  "hint": "gak ada hint",
  "story":"Sebelum pulang, Oci ingin membeli kenang-kenangan khas Lombok.\nBantu dia menemukan benda-benda ini di toko oleh-oleh!",
  "folder_dest": "Level26TokoSouvenir",
  "objectToFind": 10,
  "time_game":60, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level26TokoSouvenir
};

var level27 = {
  "levelTitle": "level 12 Pantai",
  "levelName": "level 12 Pantai",
  "hint": "gak ada hint",
  "story":"Sebelum pulang, Oci ingin menikmati pantai Lombok yang sangat indah sekali lagi.\nDi sana ia berjemur dan berenang di laut.\nBisakah kamu menemukan benda-benda ini di pantai tersebut?",
  "folder_dest": "Level27Pantai",
  "objectToFind": 10,
  "time_game":60, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level27Pantai
};

var level28 = {
  "levelTitle": "level 13 Villa",
  "levelName": "level 13 Villa",
  "hint": "gak ada hint",
  "story":"Sudah saatnya Oci packing untuk pulang.\nBantu dia packing supaya tidak ada yang ketinggalan!",
  "folder_dest": "Level28Villa",
  "objectToFind": 10,
  "time_game":60, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level28Villa
};


var level29 = {
  "levelTitle": "level 14 Airport",
  "levelName": "level 14 Airport",
  "hint": "gak ada hint",
  "story":"Oci sudah di bandara dan akan segera melewati pos pemeriksaan barang.\nOci yang ceroboh butuh bantuanmu supaya jangan ada barangnya yang tertinggal!",
  "folder_dest": "Level29AirPort",
  "objectToFind": 10,
  "time_game":60, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level29AirPort
};

var level30 = {
  "levelTitle": "level 15 Koper",
  "levelName": "level 15 Koper",
  "hint": "gak ada hint",
  "story":"Rupanya Oci terlambat mengambil bagasi, \nsehingga kopernya sudah ditumpuk bersama koper-koper lain yang juga telat diambil!\nBanyak barang-barang Oci yang terselip di antara tumpukan koper. Bisakah kamu bantu mencarinya?",
  "folder_dest": "Level30Koper",
  "objectToFind": 10,
  "time_game":60, // in second
  "win_coin":150, // coin yang didapat jika menang
  "totalObject":20,
  "objectData": level30Koper
};

levelSetting.push(level1);
levelSetting.push(level2);
levelSetting.push(level3);
levelSetting.push(level4);
levelSetting.push(level5);
levelSetting.push(level6);
levelSetting.push(level7);
levelSetting.push(level8);
levelSetting.push(level9);
levelSetting.push(level10);
levelSetting.push(level11);
levelSetting.push(level12);
levelSetting.push(level13);
levelSetting.push(level14);
levelSetting.push(level15);
levelSetting.push(level16);
levelSetting.push(level17);
levelSetting.push(level18);
levelSetting.push(level19);
levelSetting.push(level20);
levelSetting.push(level21);
levelSetting.push(level22);
levelSetting.push(level23);
levelSetting.push(level24);
levelSetting.push(level25);
levelSetting.push(level26);
levelSetting.push(level27);
levelSetting.push(level28);
levelSetting.push(level29);
levelSetting.push(level30);
