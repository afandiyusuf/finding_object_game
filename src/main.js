
//ganti jadi ".." kalau API di server sendiri
var ssoUrl = "../web/auth/login";
var development = false;
var apiUrl = '..';
var nextUrl = "";
var LevelDevelopment = false;

if(development)
{
  nextUrl = "https://tandafuntastrip.com/games/cariin_dong_staging/index.php";
}else{
  nextUrl = "https://tandafuntastrip.com/games/cariin_dong/index.php";
}

var tkn;
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}




$(document).ready(function() {

  $('input').focus(function(){
    if($(this).attr('placeholder')&& $(this).val()!=""){
      $('.ph').fadeOut();
      $(this).after('<div class="ph">'+$(this).attr('placeholder')+'</div>');
      $(this).parent().find('.ph').fadeIn();
    }
  });
  $('input').keyup(function(){
    if($(this).attr('placeholder')&&$(this).val!=""){
      $(this).after('<div class="ph">'+$(this).attr('placeholder')+'</div>');
      $(this).parent().find('.ph').fadeIn();
    }
  });
  $('input').blur(function(){
    $('.ph').fadeOut();
  });

  $("#register-btn").click(function(){
    $("#register-form").css("display","table");
    $("#login-form").css("display","none");
    $("#header").css("display","none");
  });

  $("#login-btn").click(function(){
    doLogin();
  });
  $("#close-button-info").click(function(){
    HideHintPanel();
  });
  $("#close-button-coin").click(function(){
    HidePanelAddCoin();
  })

  $("#guest-btn").click(function(){
    loginGuest();
  });

  $("#log-back").click(function(){
    $("#header").css("display","block");
    $("#login-form").css("display","none");
    $("#register-form").css("display","none");
  });
  $("#reg-back").click(function(){
    $("#header").css("display","block");
    $("#login-form").css("display","none");
    $("#register-form").css("display","none");
  });

  function emptyRegisterForm()
  {
    $("#reg-username").val("");
    $("#reg-fullname").val("");
    $("#reg-password").val("");
    $("#reg-password-conf").val("");
    $("#reg-email").val("");
  }

  $("#reg-submit").click(function(){

    var username_txt = $("#reg-username").val();
    var fullname_txt = $("#reg-fullname").val();
    var password_txt = $("#reg-password").val();
    var password_conf_txt = $("#reg-password-conf").val();
    var email_txt = $("#reg-email").val();

    if(password_txt != password_conf_txt)
    {
      alert("Password tidak sama");
      $("#reg-password").val("");
      $("#reg-password-conf").val("");
    }

    if(!validateEmail(email_txt))
    {
      $("#reg-email").val("email tidak valid");
    }

    var data = { username : username_txt, full_name : fullname_txt, email : email_txt, password : password_txt}
    var saveData = $.ajax({
      type: 'POST',
      url: apiUrl+"/api/auth/register",
      data: data,
      dataType: "JSON",
      success: function(resultData) {
        if(resultData.status == false)
        {
          if (resultData.message)
          alert(resultData.message);
          else
          alert("USERNAME/PASSOWRD ERROR");
        }else{
          $.ajaxSetup({
            headers: { 'token': resultData.data.token }
          });
          $("#credPanel").css("display","none");
          $("#header").css("display","none");
          $("#game-canvas").css("display","table");
          game.state.start("MainMenu");
        }
      }
    });
    saveData.error(function() { alert("Data Error"); });
  });


  $("#log-submit").click(function(){
    var username_txt = $("#log-username").val();
    var password_txt = $("#log-password").val();

    var data = { username : username_txt, password : password_txt}
    var saveData = $.ajax({
      type: 'POST',
      url: apiUrl+"/api/auth/login",
      data: data,
      dataType: "JSON",
      success: function(resultData) {
        if(resultData.status == false)
        {
          if (resultData.message)
          alert(resultData.message);
          else
          alert("USERNAME/PASSOWRD ERROR");
        }else{
          $.ajaxSetup({
            headers: { 'token': resultData.data.token }
          });
          $("#credPanel").css("display","none");
          $("#header").css("display","none");
          $("#game-canvas").css("display","table");
          game.state.start("MainMenu");
        }
      }
    });
  });
});

var loginPopup = false;

function doLogin() {
  loginPopup = window.open(ssoUrl);


  window.doneLogin = function(data) {

    if (loginPopup) {
      loginPopup.close();
      loginPopup = false;



      $.ajaxSetup({
        headers: { 'token': data.token }
      });
      $("#credPanel").css("display","none");
      $("#header").css("display","none");
      $("#game-canvas").css("display","table");
      game.state.start("MainMenu");
    }
  }

  if(development == true)
  {
    window.doneLogin({token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxMyIsInRrbiI6Ik4xS0F5bEs0NDMxMyJ9.oWJRp0f1Sx9qahzXpxtwFbYXr4wQQ7TUITTzLxXiX0k"});
  }

  window.cancelLogin = function() {
    if (loginPopup) {
      loginPopup.close();
      loginPopup = false;
    }
  }
}
