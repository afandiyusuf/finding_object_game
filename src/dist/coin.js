$(function(){
	$("#warning-modal").modal('show');
	cssLoad();

	if(isMobile)
	{
		$('#black-background').css('font-size','1em');
	}else {
		$('#black-background').css('font-size','2em');
	}

	$("#invis-close-btn").bind('click',function(){

		// $('.wrap').css("display","none");
		$("#black-background").css("display","none");
		$("#game-canvas").css("display","block");
	});
	$("#btn-ok").bind('click',function(){

		// $('.wrap').css("display","none");
		$("#black-background").css("display","none");
			$("#game-canvas").css("display","block");
	});

});

function ShowInfoPanel(infos)
{
	$("#form-tanda-360").css("display","none");
	$("#black-background").css("display","block");
}

function ShowinputAtm()
{
	$("#form-tanda-360").css("display","none");
	$("#black-background").css("display","block");
}
var x1 = 0;
var y1 = 0;
var x2 = 0;
var y2 = 0;

$(window).resize(function(){
	cssLoad();
});

function initSly(){
	$("#addcoin-container").css("display","block");

	var $frame  = $('#addcoin-content');
	var $slidee = $frame.children('.clearfix').eq(0);
	var $wrap   = $frame.parent();
	// Call Sly on frame
	$frame.sly({
		horizontal: 1,
		itemNav: 'basic',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: 0,
		scrollBy: 1,
		activatePageOn: 'click',
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1
	});



	$('.coin-image-container').css('width',$('.coin-image').css('width'));
	$('.coin-image-container').css('margin-left','2px');

		if(!isMobile)
		{
			$('.coin-image').mousedown(function(evt){
        x1 = evt.pageX;
        y1 = evt.pageY;
				animationDown($(this));
			});

			$('.coin-image').mouseup(function(evt){
        x2 = evt.pageX;
        y2 = evt.pageY;
        animationUp($(this));
			});
			$('.coin-image').mouseout(function(){
				animationUp($(this));
			});
		}
}

function cssLoad() {
	/* CSS */
	var canvasHeight = $('div#game-canvas canvas').height();
	var canvasWidth = $('div#game-canvas canvas').width();

	$('div#game-canvas .wrap').css({'width':canvasWidth, 'height':canvasHeight, 'margin-left':-(canvasWidth / 2)});

	// if(isMobile)
	// {
	// 	$(".wrap-1").css('padding','18% 18% 10% 17%');
	// }else{
		$("#close-button-coin").css({"margin-top":"-13%","margin-right":"-12%"});

	
	//}

	$('.coin-image').css('height',(canvasHeight/100*45)+'px');
}
