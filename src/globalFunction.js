var overlay_bg;
var addCoin_panel;
var totalAddImage = 8;
var indexButton = 0;
var allImageAddCoin = [];
var leftButton;
var rightButton;
var currentIndex = 0;
var exitButton;
var bgloading;
var loadingText;
var storyText;
var titleLoading;

function ShowOverlayBg()
{
	GlobOverlayBG = this.game.add.sprite(0,0,'LevelSelect','10x10.png');
	GlobOverlayBG.alpha = 0.8;
	GlobOverlayBG.tint = 0x3D0000;
	GlobOverlayBG.width = this.game.width;
	GlobOverlayBG.height = this.game.height;
	GlobOverlayBG.inputEnabled = true;
}

function DestroyOverlayBg()
{
		GlobOverlayBG.destroy();
}
function ShowHintPanel(){
	ShowOverlayBg();
	$("#hint-container").css("display","block");
}

function HideHintPanel(){
	DestroyOverlayBg();
	$("#hint-container").css("display","none");
}

function cekLelang()
{
	var ajaxReq = $.ajax({
		type: 'GET',
		url: apiUrl+"/api/ho/game/can-lelang",
		dataType: "JSON",
		success: function(resultData) {
			//console.log("cek lelang ",resultData);
			$("#info-message").css("display","block");
			console.log(resultData);

			if(resultData.status == true || resultData.message == "no_lelang")
			{
				console.log(resultData);
				$('#btn-lelang-3').unbind("click").bind("click",function(){
					if(x1 != x2)
						return;
					AddLelangCoin();
					});
			}else{
				$('#btn-lelang-3').unbind("click");
				$("#btn-lelang-3").attr("src","asset/add_coin/lelang tanda poin 3 off.png");
			}
		}
	});
}

function cekBonus()
{
	var ajaxReq = $.ajax({
		type: 'GET',
		url: apiUrl+"/api/ho/game/can-bonus",
		dataType: "JSON",
		success: function(resultData) {
			$("#info-message").css("display","block");
			if(resultData.status == true || resultData.message == "no_bonus")
			{

				////console.log(resultData);
				$('#btn-bonus-3').unbind("click").bind("click",function(){
					//console.log("here bos");
					if(x1 != x2 && !isMobile)
						return;

					showBlackBg();
					AddBonusCoin();
					//$("#loading-content").css("display","block");
						//AddLelangCoin();
					});
			}else{
				$('#btn-bonus-3').unbind("click");
				$("#btn-bonus-3").attr("src","asset/add_coin/bonus tanda poin 3 off.png");
			}
		}
	});
}
function cekTaka()
{
	var ajaxReq = $.ajax({
		type: 'GET',
		url: apiUrl+"/api/ho/game/can-taka",
		dataType: "JSON",
		success: function(resultData) {
			//console.log(resultData);
			if(resultData.status == true)
			{
				$('#btn-taka').attr("src","asset/add_coin/buka taka.png");
				$('#btn-taka').unbind("click").bind("click",function(){

					if(x1 != x2 && !isMobile)
						return;
							console.log("clicked2");

						showBlackBg();
						//showFormAtm();
						//ShowInfoPanel("askjdhas daskjd akjd asd123123 1k23 1k23 h1k23");
						addCoinTaka();
					});
			}else{
				$('#btn-taka').unbind("click");
				$('#btn-taka').attr("src","asset/add_coin/buka taka off.png");
			}
		}
	});
}

function addCoinTaka()
{
	showFormRekening();
}

function ShowPanelAddCoin(){
	game.scale.stopFullScreen();

	if(!coinIsInit)
	{
		initSly();
		coinIsInit = true;
	}

	ShowOverlayBg();
	$("#addcoin-container").css("z-index","999");

	//console.log("tanda 360 is "+IsReedem("tanda360"));

	if(IsReedem("tanda360"))
	{
		$("#btn-360").attr("src","asset/add_coin/buka tabungan tanda 360 off.png");
	}else{
		$("#btn-360").attr("src","asset/add_coin/buka tabungan tanda 360.png");
		$('#btn-360').unbind("click").bind("click",function(){



			if(x1 != x2)
        return;

			$("#black-background").css("display","block");
			$("#black-background").css("z-index","10000");
			$("#loading-content").css("display","block");
			$("#info-group").css("display","none");


			AddTanda360Coin();
		});
	}
	cekLelang();
	cekBonus();
	cekTaka();


}

function AddLelangCoin(){


	var ajaxReq = $.ajax({
		type: 'POST',
		url: apiUrl+"/api/ho/game/coin",
		data:{
			origin:"lelang"
		},
		dataType: "JSON",
		success: function(resultData) {
			$("#info-message").css("display","block");
			if(resultData.status == true)
			{
				totalKoin_ = resultData.data.total_coin;
				textCoin.text = totalKoin_+"";
				$("#btn-lelang-3").attr("src","asset/add_coin/lelang tanda poin 3 off.png");
				$("#btn-lelang-3").unbind("click");
				showPanelInfo("Selamat! Anda berhasil<br>mendapatkan bonus koin!");
			}else{
				showPanelInfo("Anda belum melakukan lelang <br> di periode ini");
			}
		}
	});
}

function AddBonusCoin(){
	var ajaxReq = $.ajax({
		type: 'POST',
		url: apiUrl+"/api/ho/game/coin",
		data:{
			origin:"bonus"
		},
		dataType: "JSON",
		success: function(resultData) {
			$("#info-message").css("display","block");
			if(resultData.status == true)
			{
				totalKoin_ = resultData.data.total_coin;
				textCoin.text = totalKoin_+"";
				$("#btn-bonus-3").attr("src","asset/add_coin/bonus tanda poin 3 off.png");
				$("#btn-bonus-3").unbind("click");
				showPanelInfo("Selamat! Anda berhasil<br>mendapatkan bonus koin!");
			}else{
				showPanelInfo("Anda belum berhak mendapatkan <br> bonus di periode ini");
			}
		}
	});
}


function HidePanelAddCoin(){
	DestroyOverlayBg();
	$("#addcoin-container").css("z-index","-999");
}

function ShowIndexAddCoin(index)
{
	for(i=0;i<allImageAddCoin.length;i++)
	{
		allImageAddCoin[i].destroy();
	}

	allImageAddCoin = [];

	for(i=0;i<3 && ((index*3)+i) < 1;i++)
	{
		//console.log(((index*3)+i));
		var xCor = (i%3) * 450;
		if(i == 0 && IsReedem("tanda360")){
				var addCoinImage = game.add.button(xCor + 530,580,'AddCoinScreen',null,this,'tambah '+((index*3)+i+1)+' off.png','tambah '+((index*3)+i+1)+' off.png','tambah '+((index*3)+i+1)+' off.png');
				addCoinImage.inputEnabled = false;
		}else{
				var addCoinImage = game.add.button(xCor + 530,580,'AddCoinScreen',addCoinFunction,this,'tambah '+((index*3)+i+1)+'.png','tambah '+((index*3)+i+1)+'.png','tambah '+((index*3)+i+1)+'.png');
		}

		addCoinImage.anchor.setTo(0.5,0.5);
		addCoinImage.indexThis = ((index*3)+i+1);
		AddSimpleButtonAnimation(addCoinImage,1);
		allImageAddCoin.push(addCoinImage);
	}
}

function addCoinFunction(evt)
{
	//console.log(evt.indexThis);
	var OriginString = "";
	if(evt.indexThis == 1)
	{
		OriginString = "tanda360";
		AddTanda360Coin();
	}
}

function NextAddCoin()
{
	leftButton.inputEnabled = true;
	if(!isMute && SfxUiClick2 != undefined)
	SfxUiClick2.play();

	currentIndex++;
	if(currentIndex == 1)
	{

		rightButton.alpha = 0;
		rightButton.inputEnabled = false;

	}else{

		rightButton.alpha = 1;
	}
	leftButton.alpha = 1;
	ShowIndexAddCoin(currentIndex);
}

function BefAddCoin()
{
	if(!isMute && SfxUiClick2 != undefined)
	SfxUiClick2.play();
	currentIndex--;
	if(currentIndex ==0)
	{
		rightButton.inputEnabled = true;
		leftButton.inputEnabled = false;
		leftButton.alpha = 0;
	}
	rightButton.alpha = 1;
	ShowIndexAddCoin(currentIndex);
}

function DisableAllAddCoin()
{
	if(!isMute && SfxUiClick2 != undefined)
	SfxUiClick2.play();
	this.addCoinImage.inputEnabled = true;
	overlay_bg.destroy();
	addCoin_panel.destroy();
	// leftButton.destroy();
	// rightButton.destroy();
	exitButton.destroy();
	for(i=0;i<allImageAddCoin.length;i++)
	{
		allImageAddCoin[i].destroy();
	}
}

function down(evt){
	evt.scale.setTo((evt.initialScale/10)*8,(evt.initialScale/10)*8);
}
function over(evt){
	//evt.scale.setTo(0.8,0.8);
}
function out(evt){
	evt.scale.setTo(evt.initialScale,evt.initialScale);
}
function up(evt){
	evt.scale.setTo(evt.initialScale,evt.initialScale);
}

function AddSimpleButtonAnimation(button,initialScale){
	button.initialScale = initialScale;
	// button.scale.setTo(initialScale);
	button.onInputOver.add(over, this);
	button.onInputOut.add(out, this);
	button.onInputUp.add(up, this);
	button.onInputDown.add(down,this);
}

function 	makeDragAble(obj) {
	obj.inputEnabled = true;
	obj.input.enableDrag();
	obj.events.onDragStop.add(this.onDragStop, this);
}
function	onDragStop(sprite, pointer) {
	//console.log(sprite.x,sprite.y);

}
var coinStyle = {
	font: "24px Lato",
	fill:"#FFFFFF",
	boundsAlignH:"center",
	boundsAlignV:"middle"
}

var style_object = {
	font: "20px Lato",
	fill: "#FFFFFF",
	boundsAlignH: "center",
	boundsAlignV: "top",
	fontWeight: "300",
	fontStyle:'normal'
}
var style = {
	font: "18px Lato",
	fill: "#FFFFFF",
	boundsAlignH: "left",
	boundsAlignV: "middle",
	fontWeight: "300",
	fontStyle:'normal'
}
var sub_hint_text_style = {
	font: "16px Lato",
	fill: "#FFFFFF",
	boundsAlignH: "left",
	boundsAlignV: "middle",
	fontWeight: "300",
	fontStyle:'normal'
}
var ingameTimeStyle = {
	font: "32px Lato",
	fill:"#FFFFFF",
	boundsAlignH:"center",
	boundsAlignV:"middle"
}
var loading_style = {
	font: "30px Lato",
	fill: "#FFFFFF",
	boundsAlignH: "center",
	boundsAlignV: "top",
	align:"center"
}
var info_style = {
	font: "12px Lato",
	fill: "#FFFFFF",
	boundsAlignH: "center",
	boundsAlignV: "top",
	fontWeight: "300",
	fontStyle:'normal'
}

function ShowLoading(story,_levelTitle){
	bgloading = game.add.sprite(0,0,'LOADING_SCREEN','background loading.jpg');
	loadingText = game.add.bitmapText(game.width/2+20, game.height/2+150, 'archieve',"loading...0%",40);
	if(story != undefined){
		storyText = game.add.text(game.width/2+20,810,story,loading_style);
		storyText.anchor.setTo(0.5,0);
	}
	if(_levelTitle != undefined)
	{
			titleLoading = game.add.bitmapText(game.width/2+20,770,'archieve',_levelTitle.toLowerCase());
			titleLoading.anchor.setTo(0.5,0);
	}
	loadingText.anchor.setTo(0.5,0.5);
	bgloading.width = game.width;
	bgloading.height = game.height;
}

function HideLoading(){
	bgloading.inputEnabled = false;
	bgloading.destroy();
	if(storyText != undefined)
	storyText.destroy();
	if(titleLoading != undefined)
	titleLoading.destroy();

	loadingText.destroy();
}

function loadStart(){

}
function fileComplete(progress, cacheKey, success, totalLoaded, totalFiles){
	if(loadingText != undefined)
		loadingText.text = "loading..."+progress+"%"
}
function loadComplete(){

}

var GlobOverlayBG;
var GlobInfoPanel;
var GlobInfoText;
var GlobCloseButton;
var coinText;

function ShowWarning(infoString){
	DestroyWarning();

	if(!isMute && SfxUiClick2 != undefined)
	SfxUiClick2.play();

	GlobOverlayBG = this.game.add.sprite(0,0,'LevelSelect','10x10.png');
	GlobOverlayBG.alpha = 0.8;
	GlobOverlayBG.tint = 0x3D0000;
	GlobOverlayBG.width = this.game.width;
	GlobOverlayBG.height = this.game.height;
	GlobOverlayBG.inputEnabled = true;
	GlobInfoPanel = this.game.add.sprite(this.game.width/2,this.game.height/2,'ContainerInfo');
	GlobInfoPanel.anchor.setTo(0.5,0.5);
	GlobCloseButton = this.game.add.button(1700,245,'LevelSelect',DestroyWarning,this,'button close.png','button close.png');
	GlobCloseButton.anchor.setTo(0.5,0.5);
	GlobInfoText = game.add.bitmapText(956, game.world.centerY+30, 'archieve',infoString.toLowerCase(),22);
	GlobInfoText.anchor.set(0.5);
	GlobInfoText.wordWrap = true;
	GlobInfoText.align = "center";
	GlobInfoText.wordWrapWidth = window.innerWidth - 50;
	GlobInfoText.inputEnabled = true;
	AddSimpleButtonAnimation(GlobCloseButton,1);
}

function DestroyWarning()
{
	if(!isMute && SfxUiClick != undefined)
	SfxUiClick2.play();

	if(GlobOverlayBG != undefined)
	GlobOverlayBG.destroy();

	if(GlobInfoPanel != undefined)
	GlobInfoPanel.destroy();

	if(GlobInfoText != undefined)
	GlobInfoText.destroy();

	if(GlobCloseButton != undefined)
	GlobCloseButton.destroy();

	if(coinText != undefined)
	coinText.text = totalKoin_;
}

//cekAddCoinHistory
var historyIsSet = false;
function CekAddCoinHistory(){

	var ajaxReq = $.ajax({
		type: 'GET',
		url: apiUrl+"/api/ho/game/origins",
		dataType: "JSON",
		success: function(resultData) {
			HistoryAddCoin = resultData.data;
			historyIsSet =true;
			//console.log(HistoryAddCoin);
		}
	});
}

function IsReedem(strings)
{
	if(!historyIsSet)
		return;
	if(HistoryAddCoin == undefined)
		return;

	for(var i=0;i<HistoryAddCoin.length;i++)
	{
		if(HistoryAddCoin[i] == strings)
		{
			return true;
			break;
		}
	}

	return false;
}


function 	AddDailyCoin(){

	var ajaxReq = $.ajax({
		type: 'POST',
		url: apiUrl+"/api/ho/game/coin",
		data:{
			origin:"daily"
		},
		dataType: "JSON",
		success: function(resultData) {
			//console.log(resultData);
			if(resultData.status == true)
			{
				totalKoin_ = resultData.data.total_coin;
				ShowWarning("Selamat! Anda mendapatkan 5 koin dari login harian");
			}
		}
	});

	}

	function 	AddStartCoin(){

		var ajaxReq = $.ajax({
			type: 'POST',
			url: apiUrl+"/api/ho/game/coin",
			data:{
				origin:"start"
			},
			dataType: "JSON",
			success: function(resultData) {
				//console.log(resultData);
				if(resultData.status == true)
				{
					totalKoin_ = resultData.data.total_coin;
					ShowWarning("Selamat! Anda mendapatkan 100 koin karena berhasil register game ini");
				}
			}
		});

		}

	function AddLikeFbPageCoin(){
		var ajaxReq = $.ajax({
			type: 'POST',
			url: apiUrl+"/api/ho/game/coin",
			data:{
				origin:"like"
			},
			dataType: "JSON",
			success: function(resultData) {
				//console.log(resultData);
				if(resultData.status == true)
				{
					totalKoin_ = resultData.data.total_coin;
					ShowWarning("Selamat! Anda mendapatkan koin 50 karena telah like fb page kami");
				}
			}
		});
	}

	function AddCoinFromShareLevel(){
		var ajaxReq = $.ajax({
			type: 'POST',
			url: apiUrl+"/api/ho/game/coin",
			data:{
				origin:"sharefb_"+currentLevel
			},
			dataType: "JSON",
			success: function(resultData) {
				//console.log(resultData);
				if(resultData.status == true)
				{
					totalKoin_ = resultData.data.total_coin;
					ShowWarning("Share berhasil");
				}
			}
		});
	}
	function AddCoinFromShareGame(){
		var ajaxReq = $.ajax({
			type: 'POST',
			url: apiUrl+"/api/ho/game/coin",
			data:{
				origin:"sharefb"
			},
			dataType: "JSON",
			success: function(resultData) {
				//console.log(resultData);
				if(resultData.status == true)
				{
					totalKoin_ = resultData.data.total_coin;
					ShowWarning("Anda mendapatkan 20 koin karena telah berhasil share game ini");
				}
			}
		});
	}

function AddCoinFromYoutube(){
	var ajaxReq = $.ajax({
		type: 'POST',
		url: apiUrl+"/api/ho/game/coin",
		data:{
			origin:"youtube"
		},
		dataType: "JSON",
		success: function(resultData) {
			//console.log(resultData);
			if(resultData.status == true)
			{
				totalKoin_ = resultData.data.total_coin;
				ShowWarning("Anda mendapatkan 20 koin karena telah subscribe channel kami");
				HistoryAddCoin.push("youtube");
			}
				//console.log(resultData);
		}

	});
}

function AddCounFromLike(){
	var ajaxReq = $.ajax({
		type: 'POST',
		url: apiUrl+"/api/ho/game/coin",
		data:{
			origin:"like"
		},
		dataType: "JSON",
		success: function(resultData) {
			//console.log(resultData);
			if(resultData.status == true)
			{
				totalKoin_ = resultData.data.total_coin;
				ShowWarning("Anda mendapatkan 20 koin karena telah like fb kami");
				HistoryAddCoin.push("like");
			}

		}
	});
}

function AddCoinFromFollow(){
	var ajaxReq = $.ajax({
		type: 'POST',
		url: apiUrl+"/api/ho/game/coin",
		data:{
			origin:"sharetw"
		},
		dataType: "JSON",
		success: function(resultData) {
			//console.log(resultData);
			if(resultData.status == true)
			{
				totalKoin_ = resultData.data.total_coin;
				ShowWarning("Anda mendapatkan 20 koin karena telah follow twitter kami");
				HistoryAddCoin.push("sharetw");
			}
		}
	});
}
	function OpenYoutubeChannel(){
		var win = window.open("https://www.youtube.com/c/tandafuntastrip", '_blank');
  	win.focus();
		AddCoinFromYoutube();
	}

	function OpenFbPage(){
		var win = window.open("https://www.facebook.com/TandaFuntastrip/", '_blank');
  	win.focus();
		AddCounFromLike();
	}

	function OpenTwitterPage(){
		var win = window.open("https://twitter.com/TandaFuntastrip", '_blank');
  	win.focus();
		AddCoinFromFollow();
	}

	function showBlackBg()
	{
		//$("#info-message").css("display","none");
		$("#black-background").css({
			"display":"block"
		});

	}

	//addcoin html
	function animationDown(_this)
	{
		_this.css({
		"zoom":"0.95",
		"margin": "0.5%",
		"margin-top": "2%"});
	}
	function animationUp(_this)
	{
		_this.css({
		"zoom":"1",
		"margin-left": "0",
		"margin-top": "0"});
	}

	function showBlackBg()
	{
		$("#black-background").css({
			"width":"100%",
			"height":"100%",
			"background-color": "rgba(61,0,0,0.8)",
			"position":"absolute",
			"top": 0,
			"z-index":10000,
			"display":"block"
		});
	}

	function destroyBlackBg()
	{
			$("#black-background").css("display","none");
	}

	function AddTanda360Coin(){
		var ajaxReq = $.ajax({
			type: 'POST',
			url: apiUrl+"/api/ho/game/coin",
			data:{
				origin:"tanda360"
			},
			dataType: "JSON",
			success: function(resultData) {
				//console.log(resultData);
				if(resultData.status == true)
				{
					totalKoin_ = resultData.data.total_coin;
					showPanelInfo("Selamat! Anda berhasil<br>mendapatkan bonus koin!");
					$("#btn-360").attr("src","asset/add_coin/buka tabungan tanda 360 off.png");
					$("#btn-360").unbind("click");
					HistoryAddCoin.push('tanda360');
				}else{
					showFormAtm();
				}
			}
		});
	}

	function AddTanda360CoinAtm(noatm){
		var ajaxReq = $.ajax({
			type: 'POST',
			url: apiUrl+"/api/ho/game/coin",
			data:{
				origin:"tanda360",
				no_atm:noatm
			},
			dataType: "JSON",
			success: function(resultData) {
				//console.log(resultData);
				if(resultData.status == true)
				{

					totalKoin_ = resultData.data.total_coin;
					textCoin.text = totalKoin_+"";
					showPanelInfo("Selamat! Anda berhasil<br>mendapatkan bonus koin!");
					$("#btn-360").attr("src","asset/add_coin/buka tabungan tanda 360 off.png");
					$("#btn-360").unbind("click");

				}else{
					showPanelInfo("Maaf, nomor ATM tidak valid");
				}
			}
		});
	}

	function AddTakaRekening(notaka){
		var ajaxReq = $.ajax({
			type: 'POST',
			url: apiUrl+"/api/ho/game/coin",
			data:{
				origin:"taka",
				no_taka:notaka
			},
			dataType: "JSON",
			success: function(resultData) {
				//console.log(resultData);
				if(resultData.status == true)
				{
					showPanelInfo("Kami akan segera memverifikasi<br>nomor rekening Anda.");
					$("#btn-taka").attr("src","asset/add_coin/buka taka off.png");
					$("#btn-taka").unbind("click");
				}else{
					showPanelInfo("Error")
				}
			}
		});
	}

	function showPanelInfo(info)
	{
		$("#black-background").css("display","block");
		$("#black-background").css("z-index","10000");
		$("#loading-content").css("display","block");
		$("#info-group").css("display","none");

		$("#info-message").css("display","block");
		$("#loading-content").css("display","none");
		$("#form-tanda-360").css("display","none");

		$("#info-group").css("display","block");
		$("#info-content-text").css("display","block")
		$("#info-content-text").html(info);
	}
	function showFormAtm()
	{
		$("#black-background").css("display","block");
		$("#black-background").css("z-index","10000");
		$("#loading-content").css("display","block");
		$("#info-group").css("display","none");

		$("#done-atm").unbind("click").bind("click",function(){
			$("#form-tanda-360").css("display","none");
			AddTanda360CoinAtm($("#input-no-atm").val());
			$("#input-no-atm").val("");
		})

		$("#info-group").css("display","none");
		$("#info-message").css("display","block");
		$("#loading-content").css("display","none");
		$("#form-tanda-360").css("display","block");
		$("#label-form-input").html("Masukkan nomor ATM Anda");
		$("#info-content-text").css("display","none")
	}

	function showFormRekening()
	{
		$("#black-background").css("display","block");
		$("#black-background").css("z-index","10000");
		$("#loading-content").css("display","block");
		$("#info-group").css("display","none");

		 $("#done-atm").unbind("click").bind("click",function(){
			$("#form-tanda-360").css("display","none");
		 	AddTakaRekening($("#input-no-atm").val());
		 	$("#input-no-atm").val("");
		})
		$("#info-group").css("display","none");
		$("#info-message").css("display","block");
		$("#loading-content").css("display","none");
		$("#form-tanda-360").css("display","block");
		$("#label-form-input").html("Masukkan nomor rekening Anda");
		$("#info-content-text").css("display","none")
	}
