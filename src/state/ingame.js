/*global Phaser */
/*global game */
/*global scoreUser*/
/*global timerUser*/
/*global isWin*/
/*global gameLimit*/
/*global levelSetting*/
/*global totalLevel*/
/*global currentLevel*/

// NEW UPDATE PIXELNINE (ALL STYLE)

var isIngameMusicPlayed = false;
var ingame = function(game) {
};
ingame.prototype = {
  preload: function() {

    this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
    this.ingameData = levelSetting[currentLevel];
    ShowLoading(this.ingameData.story,this.ingameData.levelName);
    this.game.load.atlasJSONHash('UI', 'asset/UI/ui.png', 'asset/UI/ui.json');
    this.game.load.atlasJSONHash('LOGIN', 'asset/LoginScreen/JsonData.png', 'asset/LoginScreen/JsonData.json');
    this.game.load.atlasJSONHash('GAME_ASSET', 'asset/' + this.ingameData.folder_dest + '/JsonData.png', 'asset/' + this.ingameData.folder_dest + '/JsonData.json');

    this.game.load.image("black_bg", "asset/image/Solid_black.png");
    this.game.load.image('background_ingame','asset/'+this.ingameData.folder_dest + '/background.jpg');
    this.game.load.image("button", "asset/image/blue_button00.png");
  },
  create: function() {
    isIngame = true;

    PlayBGM();
    this.setReadyUI();
  },
  falseClickFunction:function(){

    if(!this.isStart)
    return;

    this.falseClick++;
    if(this.falseClick == 2)
    {

      this.timeNow += 2000;
      this.falseClick = 0;
      this.animTextMinus.alpha = 1;
      this.animTextMinus.y = 800;
      var tween = this.game.add.tween(this.animTextMinus)
      .to({
        alpha: 0,
        y:720
      }, 500, Phaser.Easing.Linear.None).start();
    }
  },
  startGame: function(evt) {

    PlayInGameBegin();
    HideLoading();
    //start game
    this.isStart = true;


  },
  showAddCoin:function(){
    PlayUiClick();
    this.isStart = false;
    ShowPanelAddCoin();
  },
  showHint: function(evt) {
    if(!this.isStart)
    return;

    PlayUiClick();

    if(this.ingameCoin <= 0 && this.totalUseHint != 0)
    {
      return;
    }
    this.falseClick = 0;
    this.totalUseHint++;

    if(this.totalUseHint == 1)
    {
      //free hint
    }else if(this.totalUseHint == 2)
    {
      var ajaxReq = $.ajax({
        type: 'POST',
        url: apiUrl+"/api/ho/game/coin",
        data:{
          origin:"hint"
        },
        dataType: "JSON",
        success: function(resultData) {
          console.log(resultData);
          if(resultData.status == false)
          {

          }else{
            totalKoin_ = resultData.data.total_coin;
          }
        }
      });

      this.ingameCoin -= 5;
      this.difIngameCoin -= 5;

    }else if(this.totalUseHint == 3){
      var ajaxReq = $.ajax({
        type: 'POST',
        url: apiUrl+"/api/ho/game/coin",
        data:{
          origin:"hint"
        },
        dataType: "JSON",
        success: function(resultData) {
          console.log(resultData);
          if(resultData.status == false)
          {

          }else{

          }
        }
      });

      this.difIngameCoin -= 5;
      this.ingameCoin -= 5;
      this.hint_btn.inputEnabled = false;
      this.hint_btn.tint = 0x555555;

    }else{
      return;
    }

    if(this.totalUseHint == 2 || this.totalUseHint == 3)
    {
      this.coinTextAnim.y = 80;
      this.coinTextAnim.alpha = 1;
      var tween = this.game.add.tween(this.coinTextAnim)
      .to({
        alpha: 0,
        y: 200
      }, 1500, Phaser.Easing.Linear.None).start();
    }

    //this.hint_text.text = this.totalHint;
    //disable action hint button

    this.coinText.text = this.ingameCoin;

    for (var k = 0; k < this.foundState.length; k++) {
      if (this.foundState[k] == false) {
        this.game.world.swap(this.black_bg, this.topest_z);
        this.game.world.swap(this.arrRandomObjects[k], this.reveal_hidden_object);
        this.indexShowedObject = k;
        break;
      }
    }

    //create tween make alpha 0.8 - idle 2 detik - make alpha 0 again
    var tween = this.game.add.tween(this.black_bg)
    .to({
      alpha: 0.8
    }, 500, Phaser.Easing.Linear.None)
    .to({}, 2000, Phaser.Easing.Linear.None)
    .to({
      alpha: 0
    }, 500, Phaser.Easing.Linear.None)
    .start();
    //invoke another function change z index to default position
    tween.onComplete.add(this.hideHint, this);


  },
  hideHint: function() {
    this.game.world.swap(this.black_bg, this.topest_z);
    this.game.world.swap(this.arrRandomObjects[this.indexShowedObject], this.reveal_hidden_object);
  },
  foundObject: function(evt) {

    if(!this.isStart)
    return;

    PlayUiClick();

    this.falseClick = 0;
    var alphaZeroTween = game.add.tween(evt).to({
      alpha:0
    },200, Phaser.Easing.Linear.None,false).start();
    var centerSprite = game.add.sprite(game.width/2,game.height/2,'GAME_ASSET',(evt.realIndex+1)+'b.png');
    centerSprite.alpha = 0;
    centerSprite.scale.setTo(2,2);
    centerSprite.anchor.setTo(0.5,0.5);

    //Bring object to the top
    //tween object, dari asal ke panel hint (bawah kiri)

    game.add.tween(centerSprite).to({
      alpha: 1
    },
    500, Phaser.Easing.Linear.None,false).start();
    var tweenToBot = game.add.tween(centerSprite).to({
      alpha:0
    },
    500, Phaser.Easing.Linear.None,false,1000)
    tweenToBot.start()

    tweenToBot.onComplete.add(this.checkWinState, this)
    //disable object agar tidak bisa di klik
    evt.inputEnabled = false;
    //decrement total barang

    //tambah score
    this.thisScore += this.longGameLimit - Math.floor(this.timeNow / 1000);

    //fade out teks petunjuk
    //this.score_text.text = this.thisScore;

    game.add.tween(this.arrPanelObjects[evt.randomIndex]).to({
      alpha: 0
    }, 1000, Phaser.Easing.Linear.None, true);
    //set found state
    this.foundState[evt.randomIndex] = true;

    //stop timer if hidden object all revealed
    if (this.totalBarang == 0)
    this.isStart = false;

  },
  ShowLoginState: function(){
    var bgLogin = game.add.sprite(game.width/2,game.height/2,'LOGIN','container warning.png');
    bgLogin.anchor.setTo(0.5,0.5);
    this.infoText = game.add.bitmapText(game.width/2, 490, 'archieve',"kamu harus login/register\nuntuk melanjutkan permainan",30);
    this.infoText.anchor.setTo(0.5,0);
    this.infoText.align = 'center'
    this.black_bg.alpha = 0.8;
    this.black_bg.inputEnabled = true;
    var buttonLoginDaftar = game.add.button(game.width/2,650,'LOGIN',this.loginIngame,this,'button masuk daftar.png','button masuk daftar.png','button masuk daftar.png');
    buttonLoginDaftar.anchor.setTo(0.5,0.5);
    AddSimpleButtonAnimation(buttonLoginDaftar,1);
    this.game.world.swap(this.black_bg, this.topest_z);
    PlayUiClick();
  },
  checkWinState: function() {
    this.totalBarang--;
    if (this.totalBarang <= 0) {
      this.isStart = false;
      if(isGuest)
      {
        this.ShowLoginState();
        return;
      }
      gameLimit = this.longGameLimit;

      scoreUser = this.thisScore;
      timerUser = Math.floor(this.timeNow / 1000);
      isWin = true;
      currentLevel++;

      if(currentLevel-1 == lastLevel){
        if(currentLevel != totalLevel)
        this.difIngameCoin += this.ingameData.win_coin;
      }
      difKoin_ = this.difIngameCoin;

      ajaxReq = $.ajax({
        type: 'POST',
        url: apiUrl+"/api/ho/game/score",
        dataType: "JSON",
        data:{
          id:id_,
          key:key_,
          score:scoreUser,
          coin:difKoin_
        },
        success: function(resultData) {

          if(resultData.status == false)
          {
            ShowWarning(resultData.message.toLowerCase());
          }else{
            totalKoin_ += difKoin_;
            game.state.start("ResultScreen");
            PlayGameLimit();
          }
        }
      });

    }
  },
  update: function() {
    if (!this.isStart)
    return;

    this.timeNow += this.game.time.elapsed;

    var seconds = (this.longGameLimit - Math.floor(this.timeNow / 1000)) % 60;
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    var min = Math.floor((this.longGameLimit - Math.floor(this.timeNow / 1000)) / 60);
    this.time_text.text = min + ":" + seconds;

    if (this.timeNow / 1000 > this.longGameLimit) {
      this.timeNow = this.longGameLimit;
      game.state.start("ResultScreen");
      timerUser = 0;
      scoreUser = this.thisScore;
      isWin = false;
    }
  },
  generateAll:function(){
    this.black_bg = this.game.add.sprite(0,0,'LevelSelect','10x10.png');
    this.black_bg.width = game.width;
    this.black_bg.height = game.height;
    this.black_bg.tint = 0x3D0000;
    this.black_bg.alpha = 0;

    scoreUser = 0;
    this.falseClick = 0;
    this.isUseHint = false;
    // teks info yang ada di sebelah bawah, teks teks petunjuk
    this.arrPanelObjects = [];
    // hidden object
    this.arrRandomObjects = [];
    this.isStart = false;
    //define init variable (seconds)
    this.longGameLimit = this.ingameData.time_game;
    this.thisScore = 0;
    //Create all assets
    this.timeNow = 0;
    this.allObjectData = this.ingameData.objectData;
    this.foundState = [];
    this.totalBarang = this.ingameData.objectToFind;
    this.totalUseHint = 0;

    this.ingameCoin = totalKoin_;
    this.difIngameCoin = 0;

    //generate index 1-50 | 1 - totalobjectData
    this.AllIndexArray = [];
    this.selectedIndexArray = [];

    //create background
    var background = game.add.sprite(0, 0, 'background_ingame');
    background.width = game.width;
    background.height = game.height;
    background.inputEnabled = true;
    background.events.onInputDown.add(this.falseClickFunction, this);

    this.generateHiddenObjects();
    this.suffleArray();
    this.addListenerHiddenObject();
    this.setUI();

    //create z indext for topest layer
    this.topest_z = game.add.graphics(0, 0);
    //create z index for where object to show
    this.reveal_hidden_object = game.add.graphics(0, 0);
    this.startGame();
  },
  setReadyUI: function() {
    loadingText.text = "klik untuk memulai permainan";
    bgloading.events.onInputDown.add(this.generateAll,this);
    bgloading.inputEnabled = true;
  },
  addListenerHiddenObject:function(){
    for (i = 0; i < this.totalBarang; i++) {
      this.arrRandomObjects[i].randomIndex = i;
      this.arrRandomObjects[i].inputEnabled = true;
      this.arrRandomObjects[i].events.onInputDown.add(this.foundObject, this);
      this.foundState.push(false);
    }
  },
  generateHiddenObjects: function() {
    //generate random hidden objects
    for (i = 0; i < this.allObjectData.length; i++) {
      var randomObject = game.add.sprite(this.allObjectData[i].posX, this.allObjectData[i].posY, 'GAME_ASSET', (i+ 1) + '.png');
      randomObject.realIndex = i;
      randomObject.inputEnabled = false;
      randomObject.name = this.allObjectData[i].name;
      this.arrRandomObjects.push(randomObject);
    }
  },
  suffleArray:function(){
    var leng = this.arrRandomObjects.length;
    var newArray = [];

    for(var i=0;i<leng;i++)
    {
      var randomIndex = Math.floor(Math.random()*this.arrRandomObjects.length);
      newArray.push(this.arrRandomObjects[randomIndex]);
      this.arrRandomObjects.splice(randomIndex,1);
    };

    this.arrRandomObjects = newArray;
  },
  setUI: function() {

    //define current location
    var location;
    if(currentLevel <= 14)
      location = "bali";
    else if(currentLevel >=15 && currentLevel <= 30)
      location = "lombok";

    var mainMenuButton = this.game.add.button(30,20,'UI',this.backToMainMenu,this,
    'main menu btn.png','main menu btn.png','main menu btn.png');
    mainMenuButton.anchor.setTo(0,0);
    AddSimpleButtonAnimation(mainMenuButton,1);

    var coinImage = this.game.add.sprite(this.game.width - 20,20,'UI','UI coin.png');
    coinImage.anchor.setTo(1,0);

    this.coinText = game.add.bitmapText(1766, 66, 'archieve',""+totalKoin_,40);
    this.coinTextAnim = game.add.bitmapText(1766, 80, 'archieve',"-"+hintMinusCoin,40);
    this.coinTextAnim.alpha = 0;


    //UIbawahall
    this.uiBawah = game.add.sprite(0, 0, 'UI', 'UI bawah '+location+'.png');
    this.uiBawah.anchor.setTo(0, 1);
    this.uiBawah.x = 0;
    this.uiBawah.y = game.height;

    this.titleContainer = game.add.sprite(game.width/2,20,"UI","UI level.png");
    this.titleContainer.anchor.setTo(0.5,0);

    this.level_text = game.add.bitmapText(game.width/2 + 10, 60, 'archieve',"level "+((currentLevel%15)+1),40);
    this.level_text.anchor.setTo(0.5,0);
    console.log(((currentLevel%14)+1));
    //this.sub_hint_text = game.add.text(506, 645, this.ingameData.hint, sub_hint_text_style)

    this.time_text = game.add.bitmapText(1469, 839, 'archieve',"0:0", 40);
    this.animTextMinus = game.add.bitmapText(1469, 839, 'archieve',"-2", 40);
    this.animTextMinus.alpha = 0;

    makeDragAble(this.time_text);

    this.hint_btn = game.add.sprite(0, 0, 'button');
    this.hint_btn.width = 200;
    this.hint_btn.height = 100;
    this.hint_btn.alpha = 0;
    this.hint_btn.x = game.width - (this.hint_btn.width + 10);
    this.hint_btn.y = game.height - (this.hint_btn.height + 10);

    this.hint_btn = game.add.sprite(1540, game.height, "UI", "hint button in game.png");
    this.hint_btn.anchor.setTo(0.5, 1);
    this.hint_btn.y = this.game.height;

    var levelImage = game.add.sprite(game.width,game.height,"UI","icon level "+location+".png");
    levelImage.anchor.setTo(1,1);

    for (i = 0; i < 10 && i < this.ingameData.objectToFind; i++) {
      var textObject;
      if (i >= 5) {
        textObject = game.add.bitmapText(380 + ((i - 5) * 220), 950, "archieve",this.arrRandomObjects[i].name.toLowerCase(), 24);
      }
      else {
        textObject = game.add.bitmapText(380 + (i * 220), 1010, "archieve",this.arrRandomObjects[i].name.toLowerCase(), 24);
      }
      this.arrPanelObjects.push(textObject);
    };
    this.hint_btn.events.onInputDown.add(this.showHint, this);
    this.hint_btn.inputEnabled = true;
    this.hint_btn.tint = 0xFFFFFF;


    this.likeBtn = this.game.add.button(1836,248,"ShareAsset",OpenFbPage,this,"button like.png","button like.png","button like.png");
    this.likeBtn.anchor.setTo(0.5,0.5);
    this.shareFb = this.game.add.button(1836,248,"ShareAsset",ShareGames,this,"button fb.png","button fb.png","button fb.png");
    this.shareFb.anchor.setTo(0.5,0.5);
    this.shareTw = this.game.add.button(1836,248,"ShareAsset",OpenTwitterPage,this,"button twitter.png","button twitter.png","button twitter.png");
    this.shareTw.anchor.setTo(0.5,0.5);
    this.youtube = this.game.add.button(1836,248,"ShareAsset",OpenYoutubeChannel,this,"button youtube.png","button youtube.png","button youtube.png");
    this.youtube.anchor.setTo(0.5,0.5);

    this.likeBtn.inputEnabled = false;
    this.shareFb.inputEnabled = false;
    this.shareTw.inputEnabled = false;
    this.youtube.inputEnabled = false;

    //--HIDE AND NONACTIVE SHARE BUTTON --/
    this.likeBtn.alpha = 0;
    this.shareFb.alpha = 0;
    this.shareTw.alpha = 0;
    this.youtube.alpha = 0;

    this.mainShare = this.game.add.button(1836,248,"ShareAsset",this.ToggleShare,this,"button share.png","button share.png","button share.png");
    this.mainShare.anchor.setTo(0.5,0.5);
    this.mainShare.inputEnabled = false;
    this.mainShare.alpha = 0;

    AddSimpleButtonAnimation(this.mainShare,1);
    AddSimpleButtonAnimation(this.likeBtn,1);
    AddSimpleButtonAnimation(this.shareFb,1);
    AddSimpleButtonAnimation(this.shareTw,1);
    AddSimpleButtonAnimation(this.youtube,1);

  },
  ToggleShare:function(){
    if(!this.shareShown)
    {
      this.shareShown = true;

      if(IsReedem("sharefb"))
        this.shareFb.setFrames("button fb nonactive.png", "button fb nonactive.png", "button fb nonactive.png");
      if(IsReedem("sharetw"))
        this.shareTw.setFrames("button twitter nonactive.png", "button twitter nonactive.png", "button twitter nonactive.png");
      if(IsReedem("like"))
        this.likeBtn.setFrames("button like nonactive.png", "button like nonactive.png", "button like nonactive.png");
      if(IsReedem("youtube"))
        this.youtube.setFrames("button youtube nonactive.png", "button youtube nonactive.png", "button youtube nonactive.png");

      game.add.tween(this.mainShare).to({x:1836,y:278},300,Phaser.Easing.Back.Out, true).start();
      game.add.tween(this.likeBtn).to({x:1744,y:224.5},250,Phaser.Easing.Back.Out, true).start();
      game.add.tween(this.shareFb).to({x:1730,y:306},300, Phaser.Easing.Back.Out, true).start();

      game.add.tween(this.shareTw).to({x:1787,y:371.5},350, Phaser.Easing.Back.Out, true).start();
      game.add.tween(this.youtube).to({x:1879,y:377},400, Phaser.Easing.Back.Out, true).start();
      this.likeBtn.inputEnabled = true;
      this.shareFb.inputEnabled = true;
      this.shareTw.inputEnabled = true;
      this.youtube.inputEnabled = true;

    }else{

      this.shareShown = false;
      game.add.tween(this.mainShare).to({x:1836,y:248},300,Phaser.Easing.Back.Out, true).start();
      game.add.tween(this.likeBtn).to({x:1836,y:248},250,Phaser.Easing.Back.In, true).start();
      game.add.tween(this.shareFb).to({x:1836,y:248},300, Phaser.Easing.Back.In, true).start();
      game.add.tween(this.shareTw).to({x:1836,y:248},350, Phaser.Easing.Back.In, true).start();
      game.add.tween(this.youtube).to({x:1836,y:248},400, Phaser.Easing.Back.In, true).start();
      this.likeBtn.inputEnabled = false;
      this.shareFb.inputEnabled = false;
      this.shareTw.inputEnabled = false;
      this.youtube.inputEnabled = false;

    }
	},
  makeDragAble: function(obj) {
    obj.inputEnabled = true;
    obj.input.enableDrag();
    obj.events.onDragStop.add(this.onDragStop, this);
  },
  onDragStop: function(sprite, pointer) {
    console.log(sprite.x, sprite.y);
  },
  backToMainMenu: function()
  {
    PlayUiClick();
    if(isGuest)
    {
      game.state.start("MainMenu");
    }else{
      game.state.start("LevelSelect");
    }

  },
  loginIngame:function(){
    PlayUiClick();
    window.location.href = "https://tandafuntastrip.com/login?next="+nextUrl;
    //   https://tandafuntastrip.com
    // function cekIngameWin()
    // {
    //   ajaxReq = $.ajax({
    //     type: 'POST',
    //     url: apiUrl+"/api/ho/game",
    //     dataType: "JSON",
    //     data:{
    //       level:0
    //     },
    //     success: function(resultData) {
    //
    //       if(resultData.status == false)
    //       {
    //         ShowWarning(resultData.message.toLowerCase());
    //       }else{
    //
    //         key_ = resultData.data.key;
    //         id_ = resultData.data.id;
    //         totalKoin_ = resultData.data.total_coin;
    //         ajaxReq = $.ajax
    //         ({
    //           type: 'POST',
    //           url: apiUrl+"/api/ho/game/score",
    //           dataType: "JSON",
    //           data:
    //           {
    //             id:id_,
    //             key:key_,
    //             score:0,
    //             coin:100
    //           },
    //           success: function(resultData)
    //           {
    //             if(resultData.status == false)
    //             {
    //               ShowWarning(resultData.message.toLowerCase());
    //             }else
    //             {
    //               isGuest = false;
    //               isLogin = true;
    //
    //               game.state.start("ResultScreen");
    //             }
    //           }
    //         });
    //       }
    //     }
    //   });
    // }
    // if(isLogin){
    //   game.state.start("LevelSelect");
    //   return;
    // }
    // loginPopup = window.open(ssoUrl);
    //
    //
    // window.doneLogin = function(data) {
    //   $.ajaxSetup({
    //     headers: { 'token': data.token }
    //   });
    //   console.log("LOGIN SUCCESS");
    //   if (loginPopup) {
    //     loginPopup.close();
    //     loginPopup = false;
    //
    //     //
    //     // $("#credPanel").css("display","none");
    //     // $("#header").css("display","none");
    //     // $("#game-canvas").css("display","table");
    //     isLogin = true;
    //     var ajaxReq = $.ajax({
    //       type: 'GET',
    //       url: apiUrl+"/api/ho/game/score",
    //       dataType: "JSON",
    //       success: function(resultData) {
    //         if(resultData.status == false)
    //         {
    //           ShowWarning(resultData.message.toLowerCase());
    //         }else{
    //           console.log(resultData);
    //           lastLevel = resultData.data.level.length;
    //
    //
    //           totalKoin_ = resultData.data.total_coin;
    //           currentLevel = lastLevel;
    //           isWin = true;
    //           isGuest = false;
    //           isLogin = true;
    //           cekIngameWin();
    //         }
    //         //-------------END AJAX AREA--------------------//
    //       }
    //     });
    //
    //   }
    //
    // }
    //
    // if(development == true)
    // {
    //   window.doneLogin({token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxMyIsInRrbiI6Ik4xS0F5bEs0NDMxMyJ9.oWJRp0f1Sx9qahzXpxtwFbYXr4wQQ7TUITTzLxXiX0k"});
    // }
    //
    // window.cancelLogin = function() {
    //   if (loginPopup) {
    //     loginPopup.close();
    //     loginPopup = false;
    //   }
    // }
  }

}
