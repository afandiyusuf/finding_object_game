var totalBarang = 20;
var objectToFind = 10;
var levelTitle = "level 1";
var levelName = "level 1";
var stringName =["payung","topi","kemeja","burung","diving fins","hiu","kacamata scuba","ember","pancing","pelampung","sandal","tabung","wetsuit","kerang","sunblock","bintang laut","ransel","permen","arloji","action cam"];
var folder_dest = "Level30Koper";
var hint = "tidak ada hint";
var isUseLastSetting = true;
var isUseNameSetting = true;
var levelNumber = 29;

var allRandomBarang = [];
var levelGenerator = function(game) {

};


levelGenerator.prototype = {
    preload : function(){
        this.ingameData = levelSetting[levelNumber-1];
        if(isUseLastSetting)
        {
          folder_dest = this.ingameData.folder_dest;
        }
        game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');

        this.game.load.atlasJSONHash('GAME_ASSET','asset/'+folder_dest+'/JsonData.png','asset/'+folder_dest+'/JsonData.json')
        this.game.load.image("black_bg", "asset/image/Solid_black.png");
        this.game.load.image("button", "asset/image/blue_button00.png");
        this.game.load.image("game_background",'asset/'+folder_dest+'/background.jpg');
    },
    create : function(){
        var background = game.add.sprite(0, 0, 'game_background');
        background.width = game.width;
        background.height = game.height;

        var buttonRenderText = game.add.sprite(game.width/2,game.height-80,"button");
        buttonRenderText.inputEnabled = true;
        buttonRenderText.events.onInputDown.add(this.renderString, this);
        for(var i=0;i<totalBarang;i++)
        {
            var tempImage = game.add.sprite(i*20,20,"GAME_ASSET",(i+1)+".png");
            if(isUseLastSetting && i < this.ingameData.objectData.length)
            {
              tempImage.x = this.ingameData.objectData[i].posX;
              tempImage.y = this.ingameData.objectData[i].posY;
            }else{
              tempImage.x = i*20;
              tempImage.y = 20;
            }
            tempImage.inputEnabled = true;
            tempImage.input.enableDrag();
            allRandomBarang.push(tempImage);
        }



    },
    renderString : function()
    {
        var consoleText = [];
        for(var i=0;i<totalBarang;i++)
        {
            var tempObjData = {};

            if(isUseNameSetting)
              tempObjData.name = this.ingameData.objectData[i].name;
            else
              tempObjData.name = stringName[i];

            tempObjData.posX = allRandomBarang[i].x;
            tempObjData.posY = allRandomBarang[i].y;
            consoleText.push(tempObjData);
        }
        console.log(JSON.stringify(consoleText));
    }
}
