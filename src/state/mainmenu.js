var isMainMenuMusicPlayed = false;
var isDecoded = false;
var upKey;

/*global Phaser */
var mainmenu = function(game){
	this.game = game;
};

mainmenu.prototype = {
	preload: function(){

		ShowLoading();

		this.game.load.atlasJSONHash('AddCoinScreen','asset/add_coin/AddCoin.png','asset/add_coin/AddCoin.json');

		this.game.load.image('overlay_mini','asset/10x10.png');
		this.game.load.atlasJSONHash('SplashScreen', 'asset/SplashScreen/SplashScreen.png', 'asset/SplashScreen/SplashScreen.json');
		this.game.load.bitmapFont('archieve', 'asset/font/archieve.png', 'asset/font/archieve.fnt');
		this.game.load.atlasJSONHash('LevelSelect', 'asset/LevelSelect/JsonData.png', 'asset/LevelSelect/JsonData.json');


		this.game.load.image("bali level bg",'asset/LevelSelect/bali level bg.jpg');
		this.game.load.image("ContainerInfo",'asset/LevelSelect/container info.png');
		this.game.load.image("fullscreenImage","asset/SplashScreen/fullscreen btn.png");
		this.game.load.image("fullscreenImage_off","asset/SplashScreen/exit fullscreen btn.png");

		//--Setting Asset --//
		this.game.load.image("soundImage",'asset/Sound.png');
		this.game.load.image("muteImage","asset/Sound mute.png");
		this.game.load.image("setting_button","asset/SplashScreen/setting_btn.png");
		this.game.load.image("sound_image_on","asset/SplashScreen/sound on.png");
		this.game.load.image("sound_image_off","asset/SplashScreen/sound off.png");
		this.game.load.image("hint_button","asset/SplashScreen/hint btn.png");

		this.game.load.start();
	},
	create: function(){
		if(LevelDevelopment)
		upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);

		cssLoad();
		isIngame = false;
		this.CreateUI();
		this.createSettingUI();
	},
	update: function(){
		if(LevelDevelopment)
		{
			if(upKey.isDown)
			{
				game.state.start("LevelGenerator");
			}
		}
	},
	createSettingUI :function(){

		this.hintButton = this.game.add.button(this.game.width-50,this.game.height-30,'hint_button',ShowHintPanel,this);
		this.hintButton.anchor.setTo(1,1);
		this.hintButton.alpha = 0;

		if(!isMute)
		this.soundImage = this.game.add.button(this.game.width-50,this.game.height-30,'sound_image_on',this.toggleMuteSound, this);
		else
		this.soundImage = this.game.add.button(this.game.width-50,this.game.height-30,'sound_image_off',this.toggleMuteSound, this);


		this.soundImage.anchor.setTo(1,1);
		this.soundImage.inputEnabled = true;
		this.soundImage.alpha = 0;

		this.settingButton = this.game.add.button(this.game.width-40,this.game.height-20,'setting_button',this.toggleSetting,this);
		this.settingButton.anchor.setTo(1,1);
		this.settingButton.alpha = 1;

		AddSimpleButtonAnimation(this.settingButton,1);
		AddSimpleButtonAnimation(this.hintButton,1);
	},
	toggleSetting:function(){
		// ------- Animation dari setting button (muncul dan hide)------//
		if(!this.settingShown)
		{
			this.settingShown = true;
			game.add.tween(this.hintButton).to({x:this.game.width-50,y:this.game.height-130,alpha:1},300,Phaser.Easing.Back.Out, true).start();
			game.add.tween(this.soundImage).to({x:this.game.width-50,y:this.game.height-220,alpha:1},400, Phaser.Easing.Back.Out, true).start();
			this.hintButton.inputEnabled = true;
			this.soundImage.inputEnabled = true;


		}else{
			this.settingShown = false;
			game.add.tween(this.hintButton).to({x:this.game.width-50,y:this.game.height-30,alpha:0},300, Phaser.Easing.Back.In, true).start();
			game.add.tween(this.soundImage).to({x:this.game.width-50,y:this.game.height-30,alpha:0},400, Phaser.Easing.Back.In, true).start();
			this.hintButton.inputEnabled = false;
			this.soundImage.inputEnabled = false;
		}
	},
	toggleMuteSound:function(){
		//--Sound on off ---//
		ToggleMuteSound();
		//--- Toggle sound graphic on/off ---//
		this.soundImage.destroy();
		if(!isMute)
		{
			this.soundImage = this.game.add.button(this.game.width-50,this.game.height-220,'sound_image_on',this.toggleMuteSound, this);
		}else{
			this.soundImage = this.game.add.button(this.game.width-50,this.game.height-220,'sound_image_off',this.toggleMuteSound, this);
		}
		this.soundImage.anchor.setTo(1,1);

		this.soundImage.events.onInputDown.add(this.toggleMuteSound, this);
	},
	CreateUI :function(){
		HideLoading();
		game.stage.disableVisibilityChange = true;
		game.input.touch.preventDefault = false;

		var background = this.game.add.sprite(0,0,'SplashScreen','background splash.jpg');
		background.width = this.game.width;
		background.height = this.game.height;

		this.fullScreenButton = this.game.add.sprite(game.width - 140,50,'fullscreenImage');
		this.fullScreenButton.scale.setTo(1,1);

		this.fullScreenButton.inputEnabled = true;
		this.fullScreenButton.events.onInputDown.add(this.goFullScreen, this);

		var startButton = this.game.add.button(242,
			this.game.height - this.game.height/7,
			'SplashScreen',this.loginGame,
			this,
			'play btn.png',
			'play btn.png',
			'play btn.png');
			startButton.anchor.setTo(0.5,0.5);
			AddSimpleButtonAnimation(startButton,1);

			var guestButton = this.game.add.button(542,
				this.game.height - this.game.height/7,
				'SplashScreen',this.loginGuest,
				this,
				'demo btn.png',
				'demo btn.png',
				'demo btn.png');
				guestButton.anchor.setTo(0.5,0.5);
				AddSimpleButtonAnimation(guestButton,1);
				guestButton.alpha = 0;
				guestButton.inputEnabled = false;

				var fbLikeButton = this.game.add.button(0,0,
					'SplashScreen',this.openFbForLike,
					null,
					'like btn.png',
					'like btn.png',
					'like btn.png');

					fbLikeButton.x = this.game.width - 300;
					fbLikeButton.y = this.game.height - (fbLikeButton.height/2);
					fbLikeButton.anchor.setTo(0.5,0.5);
					AddSimpleButtonAnimation(fbLikeButton,1);
					this.cekLoginSession();
				},
				cekLoginSession:function(){

				},
				queueAsset:function(){

				},
				openFbForLike:function(){
					var win = window.open("https://www.facebook.com/tandafuntastrip", '_blank');
					win.focus();
					AddLikeFbPageCoin();
				},
				loginGame:function(evt,pointer){
					PlayUiClick();
					console.log(pointer);
					if(isMobile)
					{
						game.scale.startFullScreen(false);
					}
					console.log(apiUrl+"/api/auth/token");

					$.ajax({
						type: 'GET',
						url: apiUrl+"/api/auth/token",
						dataType: "JSON",
						success: function(resultData) {
							if(resultData.status == false)
							{
								window.location.href = "https://tandafuntastrip.com/login?next="+nextUrl;
							}else{
								isGuest = false;
								DoneLogin(resultData.data);
								//-------------END AJAX AREA--------------------//
							}
						}
					});


				},
				afterCreate:function(){

				},
				goFullScreen:function(){
					this.fullScreenButton.destroy();
					if (game.scale.isFullScreen)
					{
						this.fullScreenButton = this.game.add.sprite(game.width - 140,50,'fullscreenImage');
						this.fullScreenButton.inputEnabled = true;
						this.fullScreenButton.events.onInputDown.add(this.goFullScreen, this);
						game.scale.stopFullScreen();
					}
					else
					{
						this.fullScreenButton = this.game.add.sprite(game.width - 140,50,'fullscreenImage_off');
						this.fullScreenButton.inputEnabled = true;
						this.fullScreenButton.events.onInputDown.add(this.goFullScreen, this);
						game.scale.startFullScreen(false);
					}
				},
				nextState: function(){
				},
				loginGuest:function(){

					isGuest = true;
					currentLevel = 0;
					lastLevel = 0;
					totalKoin_ = 0;

					if(isMobile) {
						game.scale.startFullScreen(false);
					}

					game.state.start("Ingame");
				}
			}

			function DoneLogin(data)
			{

				$.ajaxSetup({
					headers: { 'token': data.token }
				});

				if (loginPopup)
				loginPopup.close();

				loginPopup = false;

				var ajaxReq = $.ajax({
					type: 'GET',
					url: apiUrl+"/api/ho/game/score",
					dataType: "JSON",
					success: function(resultData) {
						if(resultData.status == false)
						{
							ShowWarning(resultData.message.toLowerCase());
						}else{

							lastLevel = resultData.data.level.length;
							totalKoin_ = resultData.data.total_coin;
							currentLevel = lastLevel;

							if(lastLevel > totalLevel)
							{
								lastLevel = totalLevel;
								currentLevel = lastLevel;
							}

							isLogin = true;
							if(isMobile) {
								game.scale.startFullScreen(false);
							}
							game.state.start("LevelSelect");
							//-------------END AJAX AREA--------------------//
						}
					}
				});
			}
			function mainMenuLogin() {

				loginPopup = window.open(ssoUrl);

				window.doneLogin = function(data) {
					$.ajaxSetup({
						headers: { 'token': data.token }
					});

					if (loginPopup)
					loginPopup.close();

					loginPopup = false;

					var ajaxReq = $.ajax({
						type: 'GET',
						url: apiUrl+"/api/ho/game/score",
						dataType: "JSON",
						success: function(resultData) {
							if(resultData.status == false)
							{
								ShowWarning(resultData.message.toLowerCase());
							}else{

								lastLevel = resultData.data.level.length;


								totalKoin_ = resultData.data.total_coin;
								currentLevel = lastLevel;

								isLogin = true;
								if(isMobile) {
									game.scale.startFullScreen(false);
								}
								game.state.start("LevelSelect");
								//-------------END AJAX AREA--------------------//
							}
						}
					});

				}

				if(development == true)
				{
					window.doneLogin({token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxMyIsInRrbiI6Ik4xS0F5bEs0NDMxMyJ9.oWJRp0f1Sx9qahzXpxtwFbYXr4wQQ7TUITTzLxXiX0k"});
				}

				window.cancelLogin = function() {
					if (loginPopup) {
						loginPopup.close();
						loginPopup = false;
					}
				}
			}
