/*global Phaser */
/*global game */
/*global scoreUser*/
/*global timerUser*/
/*global isWin*/
/*global gameLimit*/
/*global levelSetting*/
/*global totalLevel*/
/*global currentLevel*/

var winScreen = function(game) {
	this.game = game;

};

var winScoreText;
winScreen.prototype = {
	preload: function() {
		ShowLoading();
		game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
		this.game.load.bitmapFont('archieve', 'asset/font/archieve.png', 'asset/font/archieve.fnt');
		if(isWin)
		this.ingameData = levelSetting[currentLevel-1];
		else
		this.ingameData = levelSetting[currentLevel];

		this.game.load.atlasJSONHash('RESULT', 'asset/ResultScreen/JsonData.png', 'asset/ResultScreen/JsonData.json');
		this.game.load.atlasJSONHash('GAME_ASSET', 'asset/'+this.ingameData.folder_dest+'/JsonData.png', 'asset/' + this.ingameData.folder_dest + '/JsonData.json');
		this.game.load.image('background_ingame','asset/'+this.ingameData.folder_dest + '/background.jpg');
		this.game.load.image("overlay","asset/10x10.png");

		//AFIEF
		this.game.load.image('coming_soon', 'asset/ResultScreen/coming_soon.png');
	},
	create: function() {
		HideLoading();
		var background = this.game.add.sprite(0, 0, 'background_ingame');
		background.width = this.game.width;
		background.height = this.game.height;
		this.overlayBg = game.add.sprite(0, 0, 'overlay');
		this.overlayBg.width = game.width;
		this.overlayBg.height = game.height;
		this.overlayBg.tint = 0x3D0000;
		this.overlayBg.alpha = 0.8;	//

		if(isWin)
		{
			this.mainImage = this.game.add.sprite(game.width/2, 100, 'RESULT','win.png');
			this.mainImage.anchor.setTo(0.5,0);

			// this.twitterButton = this.game.add.button(964,824,'RESULT',this.shareTwitter,this,'button twitter.png','button twitter.png');
			// this.twitterButton.anchor.setTo(0,0.5);

			if(currentLevel == 15){
				//AFIEF
				this.mainImage.destroy();
				this.mainImage = game.add.sprite(game.width/2, 100, 'coming_soon');
				this.mainImage.position.x = game.width / 2 - this.mainImage.width / 2;

				this.mainMenubutton = this.game.add.button(820,844,'RESULT',this.gotoMainMenu,this,'button lanjut.png','button lanjut.png');
				this.mainMenubutton.anchor.setTo(0,0.5);

				this.fbButton = this.game.add.button(670,844,'RESULT',this.shareFb,this,'button fb.png','button fb.png');
				this.fbButton.anchor.setTo(0,0.5);

				//COMMENT BY AFIEF
				/*this.coinPlusText = game.add.text(game.width/2, 600,'Selamat Kamu berhasil membantu oci sampai level di imigrasi.',loading_style);
				this.coinPlusText.anchor.setTo(0.5,0);
				this.scoreText = game.add.text(game.width/2,660,'Score Kamu \n',loading_style);
				this.scoreText.style.font = "42px Lato";
				this.scoreText.anchor.setTo(0.5,0);*/

				if( currentLevel-1 == lastLevel) {
					lastLevel = currentLevel;
					this.scoreText = game.add.bitmapText(915, 655, 'archieve','+'+this.ingameData.win_coin,80);
				} else {
					this.scoreText = game.add.bitmapText(915, 655, 'archieve','+0',80);
				}
				this.scoreText.fontSize = 60;
				this.scoreText.position.x = game.width / 2 - 40;
				this.scoreText.position.y = 650;
				winScoreText = this.scoreText;

				this.coinIcon = game.add.sprite(0, 0, 'RESULT', 'Coin.png');
				this.coinIcon.position.set(game.width / 2 - 210, 620);


				var ajaxReq = $.ajax({

					type: 'GET',
					url: apiUrl+"/api/ho/game/score",
					dataType: "JSON",
					success: function(resultData) {
						if(resultData.status == false)
						{
							ShowWarning(resultData.message.toLowerCase());
						}else{
							var totalAllScore = 0;
							for(var i=0;i<resultData.data.level.length;i++)
							{
								totalAllScore += parseInt(resultData.data.level[i].first_score);
							}
							//winScoreText.text = 'Score Kamu \n'+totalAllScore;
							console.log(resultData);
							//-------------END AJAX AREA--------------------//
						}
					}
				});
			}else{
				this.mainMenubutton = this.game.add.button(599,824,'RESULT',this.gotoMainMenu,this,'button menu.png','button menu.png');
				this.mainMenubutton.anchor.setTo(0,0.5);
				this.fbButton = this.game.add.button(815,824,'RESULT',this.shareFb,this,'button fb.png','button fb.png');
				this.fbButton.anchor.setTo(0,0.5);
				this.lanjutButton = this.game.add.button(964,824,'RESULT',this.nextLevel,this,'button lanjut.png','button lanjut.png');
				this.lanjutButton.anchor.setTo(0,0.5);
				this.coinImage = this.game.add.sprite(760,655,'RESULT','Coin.png');
				this.coinImage.anchor.setTo(0,0.5);


				if( currentLevel-1 == lastLevel && currentLevel != totalLevel)
				this.coinPlusText = game.add.bitmapText(915, 655, 'archieve','+'+this.ingameData.win_coin,80);
				else
				this.coinPlusText = game.add.bitmapText(915, 655, 'archieve','+0',80);


				AddSimpleButtonAnimation(this.lanjutButton,1);
			}

			AddSimpleButtonAnimation(this.mainMenubutton,1);
			AddSimpleButtonAnimation(this.fbButton,1);

		}else{
			this.mainImage = this.game.add.sprite(game.width/2, 100, 'RESULT','lose.png');
			this.mainImage.anchor.setTo(0.5,0);
			this.mainMenubutton = this.game.add.button(832,659,'RESULT',this.gotoMainMenu,this,'button menu.png','button menu.png');
			this.mainMenubutton.anchor.setTo(0.5,0.5);
			this.ulangiButton = this.game.add.button(1074,659,'RESULT',this.ulangiLevel,this,'button ulangi.png','button ulangi.png');
			this.ulangiButton.anchor.setTo(0.5,0.5);
			AddSimpleButtonAnimation(this.mainMenubutton,1);
			AddSimpleButtonAnimation(this.ulangiButton,1);
		}

	},
	shareFb:function(){
		PlayUiClick();
		ShareFb();
	},
	shareTwitter:function(){
		PlayUiClick();
		var win = window.open("https://twitter.com/home?status=Yuk%20bermain%20di%20https%3A//tandafuntastrip.com", '_blank');
		win.focus();
	},
	gotoMainMenu:function(){
		PlayUiClick();

				game.state.start("LevelSelect");
		

	},
	ulangiLevel:function(){
		PlayUiClick();
		if(isMobile) {
			game.scale.startFullScreen(false);
		}
		game.state.start("Ingame");
	},
	nextLevel: function() {
		PlayUiClick();
		console.log(currentLevel);
		if(isMobile) {
			game.scale.startFullScreen(false);
		}
		ajaxReq = $.ajax({
			type: 'POST',
			url: apiUrl+"/api/ho/game",
			dataType: "JSON",
			data:{
				level:currentLevel
			},
			success: function(resultData) {
				if(resultData.status == false)
				{
					ShowWarning(resultData.message.toLowerCase());
				}else{

					key_ = resultData.data.key;
					id_ = resultData.data.id;
					totalKoin_ = resultData.data.total_coin;

					gotoSubSelectLevel = true;

					if(currentLevel==totalLevel)
					{
						game.state.start("LevelSelect");
					}else{
						lastLevel++;
						game.state.start("Ingame");
					}

				}
			}
		});
	},
	parsingTime: function(timers) {

		var seconds = (gameLimit - Math.floor(timers)) % 60;
		if (seconds < 10) {
			seconds = "0" + seconds;
		}
		var min = Math.floor((gameLimit - Math.floor(timers)) / 60);
		return  min + ":" + seconds;
	}

}
