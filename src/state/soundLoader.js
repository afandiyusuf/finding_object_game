var SfxUiClick2;
var SfxTimeLimit2;
var SfxIngameBegin2;
var BgmIngame2;
var BgmMainMenu2;
var SfxFoundObject2;
var SfxTimeUp2;
var isSoundReady = false;
var isMute = true;
var isLoaded = false;

var soundLoader = function(game) {
};

soundLoader.prototype = {
  preload: function() {
    soundGame.load.audio('ingame_begin_sfx', 'asset/Sound/SFX/ingame_begin.mp3');
    soundGame.load.audio('ingame_limit_sfx', 'asset/Sound/SFX/ingame_timelimit.mp3');
    soundGame.load.audio('ui_click_sfx', 'asset/Sound/SFX/UI_click.mp3');
    soundGame.load.audio('ingame1_music', 'asset/Sound/BGM/ingame 1.mp3');
    soundGame.load.audio('menu_music', 'asset/Sound/BGM/menu.mp3');
    soundGame.load.audio('ingame_found','asset/Sound/SFX/ingame_foundobject.mp3');
    soundGame.load.audio('ingame_timeup','asset/Sound/SFX/ingame_timeup.mp3');

  },

  create: function(){
    SfxUiClick2 = soundGame.add.audio('ui_click_sfx');
    SfxTimeLimit2 = soundGame.add.audio('ingame_limit_sfx');
    SfxIngameBegin2 = soundGame.add.audio('ingame_begin_sfx');
    BgmIngame2 = soundGame.add.audio('ingame1_music');
    BgmMainMenu2 = soundGame.add.audio('menu_music');
    SfxFoundObject2 = soundGame.add.audio('ingame_found');
    SfxTimeUp2 = soundGame.add.audio('ingame_timeup');
    soundGame.sound.setDecodedCallback([ SfxUiClick2, SfxTimeLimit2, SfxIngameBegin2, BgmIngame2,BgmMainMenu2], this.SoundReady, this);
  },

  SoundReady:function(){
    isSoundReady = true;
    PlayBGM();
  }
}

function ToggleMuteSound(){
  isMute = !isMute

  if(!isLoaded)
  {
    isLoaded = true;
    soundGame.state.start("SoundLoader");
  }else if(isSoundReady){
    if(!isMute){
      PlayBGM();
    }else{
      BgmMainMenu2.stop();
      BgmIngame2.stop();
    }
  }

}

function PlayBGM(){
  if(isSoundReady)
  {
    if(isIngame && !isMute)
    {
      BgmMainMenu2.stop();
      BgmIngame2.loopFull();
    }else if(!isIngame && !isMute){
      BgmMainMenu2.loopFull();
      BgmIngame2.stop();
    }
  }
}

function PlayUiClick(){
  if(!isSoundReady || isMute)
  return;

  SfxUiClick2.play();

}

function PlayFoundObject(){
  if(!isSoundReady || isMute)
  return;

  SfxFoundObject2.play();

}

function PlayGameLimit(){
  if(!isSoundReady || isMute)
  return;

  SfxTimeLimit2.play();
}

function PlayInGameBegin(){
  if(!isSoundReady || isMute)
  return;

  SfxIngameBegin2.play();
}

function StopBGM(){
  if(!isSoundReady)
    return;

  BgmIngame2.stop();
  BgmMainMenu2.stop();
}
