var bootState = function(game) {
};
bootState.prototype = {
  preload: function() {
    	game.load.atlasJSONHash('LOADING_SCREEN','asset/LoadingScreen/Jsondata.png','asset/LoadingScreen/Jsondata.json');
      this.game.load.bitmapFont('archieve', 'asset/font/archieve.png', 'asset/font/archieve.fnt');

      this.game.load.onLoadStart.add(loadStart, this);
      this.game.load.onFileComplete.add(fileComplete, this);
      this.game.load.onLoadComplete.add(loadComplete, this);
  },
  create: function(){
    if(isMobile)
    {
      game.input.touch.enabled = false;
      game.scale.maxWidth = window.innerWidth;
      game.scale.maxHeight = window.innerHeight;
    }else{
      game.scale.maxWidth = 1366;
      game.scale.maxHeight = 768;
    }

    game.scale.pageAlignHorizontally = true;
    game.scale.pageAlignVertically = true;
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.scale.setShowAll();
    game.scale.refresh();
    game.state.start("MainMenu");

  }
}
