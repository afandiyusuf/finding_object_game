var LevelSelect = function(game){
	this.game = game;
};
var isLevelclicked = false;
var textCoin;
LevelSelect.prototype = {
	preload: function(){
		ShowLoading();
		this.game.load.image('tambahcoin','asset/UI tambah coin.png');

		this.game.load.image('AddCoinPanel','asset/add_coin/pop up tambah koin.png');
		this.game.load.atlasJSONHash('LevelSelect', 'asset/LevelSelect/JsonData.png', 'asset/LevelSelect/JsonData.json');
		this.game.load.atlasJSONHash('ShareAsset', 'asset/ShareTexture.png', 'asset/ShareTexture.json');
		this.game.load.image("Background",'asset/LevelSelect/background main menu.jpg');
		this.game.load.image("bali level bg",'asset/LevelSelect/bali level bg.jpg');
		this.game.load.image("lombok level bg",'asset/LevelSelect/lombok level bg.jpg');
		this.game.load.image("ContainerInfo",'asset/LevelSelect/container info.png');

	},
	create: function(){
		this.settingShown = false;
		this.shareShown = false;
		isLevelclicked = false;
		isIngame = false;
		PlayBGM();
		//hide loading screen
		HideLoading();
		//create main select level (5 level area, bali, london dsb)//
		this.CreateMainSelectLevel();
		//cek user udah reedeem koin apa aja, (buat menu add koin)//
		CekAddCoinHistory();
		//cek role user, dan session user (jika user login jika tidak maka user guest dan langsung main)//
		this.CekScoreAndUserRole();
		//Set Main ui (setting button, koin, dan sharebutton)
		this.SetMainUI();
	},
	//---cek apakau user guest atau bukan, dan cek session user (terakhir main di level berapa)---//
	CekScoreAndUserRole:function(){
		console.log(isGuest);
		//--- jika ternyata guest, override buat langsung main tanpa melihat level ---//
		if(isGuest){
			game.state.start("Ingame");
		}else{
			this.cekDaily();
			//---------------------- AJAX CEK SESSION USER -------------------//
			var ajaxReq = $.ajax({
				type: 'GET',
				url: apiUrl+"/api/ho/game/score",
				dataType: "JSON",
				success: function(resultData) {
					if(resultData.status == false)
					{
						ShowWarning(resultData.message.toLowerCase());
					}else{
						console.log(resultData)

						lastLevel = resultData.data.level.length;
						//AFIEF
						if(lastLevel > totalLevel)
							lastLevel = totalLevel;

						totalKoin_ = resultData.data.total_coin;
						currentLevel = lastLevel;

						isLogin = true;
						textCoin.text = totalKoin_;
					}
				}
			});
			//---------------------- END AJAX CEK SESSION USER -------------------//
		}
	},
	CreateMainSelectLevel:function(){

		var mainStage1name = "bali level";
		var mainStage2name = "hongkong level";
		var mainStage3name = "london level";
		var mainStage4name = "lombok level";
		var mainStage5name = "tokyo level";

		var background = this.game.add.sprite(0,0,'Background');
		background.width = this.game.width;
		background.height = this.game.height;

		this.mainMenuButton = this.game.add.button(30,20,'LevelSelect',this.backToMainMenu,this,
		'main menu btn.png','main menu btn.png','main menu btn.png');
		this.mainMenuButton.anchor.setTo(0,0);
		AddSimpleButtonAnimation(this.mainMenuButton,1);

		if(isMobile)
			this.fullScreenButton = this.game.add.sprite(280,20,'fullscreenImage_off');
		else
			this.fullScreenButton = this.game.add.sprite(280,20,'fullscreenImage');

		this.fullScreenButton.inputEnabled = true;
		this.fullScreenButton.events.onInputDown.add(this.goFullScreen, this);

		this.coinImage = this.game.add.sprite(this.game.width - 20,20,'LevelSelect','coin ui.png');
		this.coinImage.anchor.setTo(1,0);


		this.coinText = game.add.bitmapText(1750, 50, 'archieve',""+totalKoin_,40);
		textCoin = this.coinText;
		this.addCoinImage = this.game.add.button(this.game.width - 50,120,'tambahcoin',ShowPanelAddCoin,this);
		this.addCoinImage.anchor.setTo(1,0);
		this.addCoinImage.alpha = 0;
		this.addCoinImage.inputEnabled = false;

		var coorX = [297,556,876,1239,1589];
		var coorY = [760,500,804,480,680];
		var stagename = [
			"bali level",
			"lombok level",
			"hongkong level locked",
			"tokyo level locked",
			"london level locked"
		];
		var stagenameLock = [
			"bali level",
			"lombok level locked",
			"hongkong level locked",
			"tokyo level locked",
			"london level locked"
		]
		for(i=0;i<stagename.length;i++)
		{
			if(i == 0)
			{
				var tempLevel = this.game.add.button(coorX[i],coorY[i]+50,'LevelSelect',this.ShowSubSelectLevel,this,stagename[i]+".png",stagename[i]+".png",stagename[i]+".png");
			}else if(i==1 && lastLevel >= 16){
				var tempLevel = this.game.add.button(coorX[i],coorY[i]+50,'LevelSelect',this.ShowSubSelectLevel,this,stagename[i]+".png",stagename[i]+".png",stagename[i]+".png");
			}else{
				var tempLevel = this.game.add.button(coorX[i],coorY[i]+50,'LevelSelect',null,this,stagenameLock[i]+".png",stagenameLock[i]+".png",stagenameLock[i]+".png");
			}
			tempLevel.lvlindex = i;
			tempLevel.anchor.setTo(0.5,0.5);
			AddSimpleButtonAnimation(tempLevel,1);
			tempLevel.alpha = 0;
			game.add.tween(tempLevel).to({
				alpha:1,
				y:coorY[i]
			},500, Phaser.Easing.Linear.None, true, i*100);
		}
		//-------------------------------------//

	},
	ToggleShare:function(){
		if(!this.shareShown)
		{

			this.shareShown = true;



			if(IsReedem("sharefb"))
				this.shareFb.setFrames("button fb nonactive.png", "button fb nonactive.png", "button fb nonactive.png");
			if(IsReedem("sharetw"))
				this.shareTw.setFrames("button twitter nonactive.png", "button twitter nonactive.png", "button twitter nonactive.png");
			if(IsReedem("like"))
				this.likeBtn.setFrames("button like nonactive.png", "button like nonactive.png", "button like nonactive.png");
			if(IsReedem("youtube"))
				this.youtube.setFrames("button youtube nonactive.png", "button youtube nonactive.png", "button youtube nonactive.png");

			game.add.tween(this.mainShare).to({x:1836,y:278},300,Phaser.Easing.Back.Out, true).start();
			game.add.tween(this.likeBtn).to({x:1744,y:224.5},250,Phaser.Easing.Back.Out, true).start();
			game.add.tween(this.shareFb).to({x:1730,y:306},300, Phaser.Easing.Back.Out, true).start();

			game.add.tween(this.shareTw).to({x:1787,y:371.5},350, Phaser.Easing.Back.Out, true).start();
			game.add.tween(this.youtube).to({x:1879,y:377},400, Phaser.Easing.Back.Out, true).start();
			this.likeBtn.inputEnabled = true;
			this.shareFb.inputEnabled = true;
			this.shareTw.inputEnabled = true;
			this.youtube.inputEnabled = true;

		}else{

			this.shareShown = false;
			game.add.tween(this.mainShare).to({x:1836,y:288},300,Phaser.Easing.Back.Out, true).start();
			game.add.tween(this.likeBtn).to({x:1836,y:288},250,Phaser.Easing.Back.In, true).start();
			game.add.tween(this.shareFb).to({x:1836,y:288},300, Phaser.Easing.Back.In, true).start();
			game.add.tween(this.shareTw).to({x:1836,y:288},350, Phaser.Easing.Back.In, true).start();
			game.add.tween(this.youtube).to({x:1836,y:288},400, Phaser.Easing.Back.In, true).start();
			this.likeBtn.inputEnabled = false;
			this.shareFb.inputEnabled = false;
			this.shareTw.inputEnabled = false;
			this.youtube.inputEnabled = false;

		}
	},
	SetMainUI:function(){
		//----- Generate button share -------//

		//main share button (toggle button)

		this.likeBtn = this.game.add.button(1836,288,"ShareAsset",OpenFbPage,this,"button like.png","button like.png","button like.png");
		this.likeBtn.anchor.setTo(0.5,0.5);
		this.shareFb = this.game.add.button(1836,288,"ShareAsset",ShareGames,this,"button fb.png","button fb.png","button fb.png");
		this.shareFb.anchor.setTo(0.5,0.5);
		this.shareTw = this.game.add.button(1836,288,"ShareAsset",OpenTwitterPage,this,"button twitter.png","button twitter.png","button twitter.png");
		this.shareTw.anchor.setTo(0.5,0.5);
		this.youtube = this.game.add.button(1836,288,"ShareAsset",OpenYoutubeChannel,this,"button youtube.png","button youtube.png","button youtube.png");
		this.youtube.anchor.setTo(0.5,0.5);

		this.likeBtn.inputEnabled = false;
		this.shareFb.inputEnabled = false;
		this.shareTw.inputEnabled = false;
		this.youtube.inputEnabled = false;

		//--HIDE AND NONACTIVE SHARE BUTTON --/
		// this.likeBtn.alpha = 0;
		// this.shareFb.alpha = 0;
		// this.shareTw.alpha = 0;
		// this.youtube.alpha = 0;

		this.mainShare = this.game.add.button(1836,288,"ShareAsset",this.ToggleShare,this,"button share.png","button share.png","button share.png");
		this.mainShare.anchor.setTo(0.5,0.5);

		this.mainShare.alpha = 1;
		AddSimpleButtonAnimation(this.mainShare,1);
		AddSimpleButtonAnimation(this.likeBtn,1);
		AddSimpleButtonAnimation(this.shareFb,1);
		AddSimpleButtonAnimation(this.shareTw,1);
		AddSimpleButtonAnimation(this.youtube,1);
		//------- END OF SHARE AREA --------------//

		//------SETTING BUTTON -----------
		this.hintButton = this.game.add.button(this.game.width-50,this.game.height-30,'hint_button',ShowHintPanel,this);
		this.hintButton.anchor.setTo(1,1);
		this.hintButton.alpha = 1;

		if(!isMute)
			this.soundImage = this.game.add.sprite(this.game.width-50,this.game.height-30,'sound_image_on');
		else
			this.soundImage = this.game.add.sprite(this.game.width-50,this.game.height-30,'sound_image_off');


		this.soundImage.anchor.setTo(1,1);
		this.soundImage.inputEnabled = true;
		this.soundImage.events.onInputDown.add(this.toggleMuteSound, this);
		this.soundImage.alpha = 1;

		this.settingButton = this.game.add.button(this.game.width-40,this.game.height-20,'setting_button',this.toggleSetting,this);
		this.settingButton.anchor.setTo(1,1);
		this.settingButton.alpha = 1;

		AddSimpleButtonAnimation(this.settingButton,1);
		AddSimpleButtonAnimation(this.addCoinImage,1);
		AddSimpleButtonAnimation(this.hintButton,1);
	},
	//--toggle setting button----//
	toggleSetting:function(){
		// ------- Animation dari setting button (muncul dan hide)------//
		if(!this.settingShown)
		{
			this.settingShown = true;
			game.add.tween(this.hintButton).to({x:this.game.width-50,y:this.game.height-130},300,Phaser.Easing.Back.Out, true).start();
			game.add.tween(this.soundImage).to({x:this.game.width-50,y:this.game.height-220},400, Phaser.Easing.Back.Out, true).start();

			this.hintButton.inputEnabled = true;
			this.soundImage.inputEnabled = true;


		}else{
			this.settingShown = false;
			game.add.tween(this.hintButton).to({x:this.game.width-50,y:this.game.height-30},300, Phaser.Easing.Back.In, true).start();

			game.add.tween(this.soundImage).to({x:this.game.width-50,y:this.game.height-30},400, Phaser.Easing.Back.In, true).start();

			this.hintButton.inputEnabled = false;
			this.soundImage.inputEnabled = false;
		}
	},
	//---toggle sound, sound on - off ----//
	toggleMuteSound:function(){
		//--Sound on off ---//
		ToggleMuteSound();
		//--- Toggle sound graphic on/off ---//
		this.soundImage.destroy();
		if(!isMute)
		{
			this.soundImage = this.game.add.sprite(this.game.width-50,this.game.height-220,'sound_image_on');
		}else{
			this.soundImage = this.game.add.sprite(this.game.width-50,this.game.height-220,'sound_image_off');
		}
		this.soundImage.anchor.setTo(1,1);
		this.soundImage.inputEnabled = true;
		this.soundImage.events.onInputDown.add(this.toggleMuteSound, this);
	},
	//--- cek bonus score dari login harian dan bonus daftar koin ---//
	cekDaily:function(){
		//--- cek bonus score dari login harian dan bonus daftar koin ---//
		AddDailyCoin();
		AddStartCoin();
		coinText = this.coinText;
	},
	// -- function for fullscreen --//
	goFullScreen:function(){
		this.ToggleGraphicFullScreen();
	},
	ToggleGraphicFullScreen(){
		this.fullScreenButton.destroy();
		if (game.scale.isFullScreen)
    {
			this.fullScreenButton = this.game.add.sprite(280,20,'fullscreenImage');
			this.fullScreenButton.inputEnabled = true;
			this.fullScreenButton.events.onInputDown.add(this.goFullScreen, this);
      game.scale.stopFullScreen();
    }
    else
    {
			this.fullScreenButton = this.game.add.sprite(280,20,'fullscreenImage_off');
			this.fullScreenButton.inputEnabled = true;
			this.fullScreenButton.events.onInputDown.add(this.goFullScreen, this);
      game.scale.startFullScreen(false);
    }

	},
	// Show info button
	ShowInfo:function(){
		PlayUiClick();
		this.overlayBackground = this.game.add.sprite(0,0,'LevelSelect','10x10.png');
		this.overlayBackground.alpha = 0.8;
		this.overlayBackground.tint = 0x3D0000;
		this.overlayBackground.width = this.game.width;
		this.overlayBackground.height = this.game.height;
		this.overlayBackground.inputEnabled = true;
		this.infoPanel = this.game.add.sprite(this.game.width/2,this.game.height/2,'ContainerInfo');
		this.infoPanel.anchor.setTo(0.5,0.5);
		this.cancelButton = this.game.add.button(1700,245,'LevelSelect',this.HideInfo,this,'button close.png','button close.png');
		this.cancelButton.anchor.setTo(0.5,0.5);
		var directions = "Aturan Bermain Game Cariin Dong!\n \t- Cari objek sesuai daftar pada setiap level sebelum waktu habis.\n \t- Setiap kali kamu meng-klik objek yang salah, akan ada penalti pengurangan waktu sebanyak 2 (dua) detik.\n \t- Apabila kamu berhasil menemukan semua benda dalam daftar sebelum waktu habis, maka kamu akan memenangkan level tersebut dan bisa melanjutkan ke level berikutnya.\n \t- Setiap kamu memenangkan level baru, kamu akan mendapatkan coin.\n \t- Pengulangan level yang sudah dilewati tidak menambahkan jumlah coin yang kamu dapat.\nHint\n \t- Kamu bisa menggunakan 3 (tiga) bantuan dalam tiap level.\n \t- Bantuan pertama bisa dipakai secara cuma-cuma, bantuan ke-dua dan ke-tiga masing-masing akan dikurangi 5 (lima) coin bila dipakai.\nBonus Coin\n\n Selain bermain Games, kamu juga bisa mendapatkan tambahan coin dengan berbagai cara lain loh! Info lengkapnya bisa di-cek di www.tandafuntastrip.com/coin";
		var style = { font: "30px Lato", fill: "#ffffff", align: "left" };
		this.infoText = game.add.text(956, game.world.centerY+30, directions, style);
		this.infoText.anchor.set(0.5);
		this.infoText.wordWrap = true;
		this.infoText.wordWrapWidth = window.innerWidth - 50;
		this.infoText.inputEnabled = true;
		this.infoText.events.onInputDown.add(this.openTandaFuntastripWeb, this);
		AddSimpleButtonAnimation(this.cancelButton,1);
	},
	openTandaFuntastripWeb:function(){
		window.open("https://www.tandafuntastrip.com/coin");
	},
	HideInfo:function(){
		PlayUiClick();
		this.overlayBackground.destroy();
		this.infoPanel.destroy();
		this.cancelButton.destroy();
		this.infoText.destroy();
	},
	ShowSubSelectLevel:function(evt)
	{
		PlayUiClick();
		switch (evt.lvlindex) {
			case 0:
			this.CallSubLevel('bali',0);
			break;
			case 1:
			this.CallSubLevel('lombok',1);
			break;
			default:
			alert("value out of range")
			break;
		}
	},
	//-- Generate sub level 15 level ----//
	CallSubLevel:function(keyName,lvlIndex){

		this.SubBg = this.game.add.sprite(0,0,keyName+' level bg');
		this.SubBg.width = game.width;
		this.SubBg.height = game.height;
		this.SubBg.inputEnabled = true;

		subTrashcan.push(this.SubBg);
		this.SubLvlTitle = this.game.add.sprite(game.width/2,20,'LevelSelect',keyName+' title.png');
		this.SubLvlTitle.anchor.setTo(0.5,0);
		subTrashcan.push(this.SubLvlTitle);
		// this.game.world.bringToTop(this.settingButton);
		this.game.world.bringToTop(this.settingButton);

		this.gotoSelectLevelBtn = this.game.add.button(30,20,'LevelSelect',this.DestroySubLvlScreen,this,
		'main menu btn.png','main menu btn.png','main menu btn.png');
		this.gotoSelectLevelBtn.anchor.setTo(0,0);
		AddSimpleButtonAnimation(this.gotoSelectLevelBtn,1);
		subTrashcan.push(this.gotoSelectLevelBtn);

		//------ membuat beberapa button berada diatas, sehingga bisa digunakan di menu sublevel ---//
		this.game.world.bringToTop(this.coinImage);
		this.game.world.bringToTop(this.coinText);
		this.game.world.bringToTop(this.addCoinImage);
		this.game.world.bringToTop(this.soundImage);
		this.game.world.bringToTop(this.hintButton);
		this.game.world.bringToTop(this.settingButton);
		this.game.world.bringToTop(this.fullScreenButton);

		this.game.world.bringToTop(this.shareFb);
		this.game.world.bringToTop(this.likeBtn);
		this.game.world.bringToTop(this.shareTw);
		this.game.world.bringToTop(this.youtube);
		this.game.world.bringToTop(this.mainShare);

		//------ generate koper 15 level dan fungsi2nya ------//
		var startIndex = lvlIndex*15
		var index = startIndex;
		// --- total baris 3 ---//
		for(i=0;i<3;i++)
		{
			// ---- total kolom --//
			for(j=0;j<5;j++)
			{
				index++;
				var subLvlBtn;
				/*-- cek last level user dan total level yang dikehendaki admin
				jika iya maka bikin koper terbuka dan bisa dimainkan---*/
					if(index <= lastLevel+1 && index <= totalLevel)
				{

					subLvlBtn = this.game.add.button(400+j*280,380+i*270,'LevelSelect',this.playGame,this,'level clear.png','level clear.png');
					var subLvlText = this.game.add.bitmapText(400+j*280,300+i*270,'archieve',index-startIndex,64);
					subLvlText.anchor.setTo(0.5,0);
					subLvlText.scale.setTo(0,0);
					subTrashcan.push(subLvlText);
					game.add.tween(subLvlText.scale).to({
						x:1,
						y:1
					},1000, Phaser.Easing.Back.Out, true, (index-startIndex)*100).start();

				}else{

					if(index <= totalLevel)
					{
							subLvlBtn = this.game.add.button(400+j*280,380+i*270,'LevelSelect',null,this,'level unclear.png','level unclear.png');
					}else{
							subLvlBtn = this.game.add.button(400+j*280,380+i*270,'LevelSelect',this.showWarningLevel,this,'level unclear.png','level unclear.png');
					}

					subLvlBtn.scale.setTo(0,0);
					var subLvlText = this.game.add.bitmapText(400+j*280,380+i*270,'archieve',index-startIndex,64);
					subLvlText.anchor.setTo(0.5,0);
					subLvlText.scale.setTo(0,0);
					subTrashcan.push(subLvlText);
					game.add.tween(subLvlText.scale).to({
						x:1,
						y:1
					},1000, Phaser.Easing.Back.Out, true, (index-startIndex)*100).start();
				}

				subLvlBtn.width = 0;
				subLvlBtn.height = 0;
				var buttonTween = game.add.tween(subLvlBtn.scale).to({
					x:1,
					y:1
				},1000, Phaser.Easing.Back.Out, true, (index-startIndex)*100).start();
				subLvlBtn.thisIndex = index-1;
				subLvlBtn.anchor.setTo(0.5,0.5);
				subTrashcan.push(subLvlBtn);
				AddSimpleButtonAnimation(subLvlBtn,1);
			}
		}
	},
	showWarningLevel:function(){
		ShowWarning("saat ini kamu hanya bisa bermain sampai level "+totalLevel+".\nnantikan level-level berikutnya di tanda funtastrip!");
	},
	playGame:function(evt){

		PlayUiClick();
		//--rekuest api untuk token game
		if(!isLevelclicked){
			isLevelclicked = true;
			currentLevel = evt.thisIndex;

			ajaxReq = $.ajax({
				type: 'POST',
				url: apiUrl+"/api/ho/game",
				dataType: "JSON",
				data:{
					level:currentLevel
				},
				success: function(resultData) {
					console.log(resultData);
					if(resultData.status == false)
					{
						isLevelclicked = false;
						ShowWarning(resultData.message.toLowerCase());
					}else{
						isLevelclicked = false;
						key_ = resultData.data.key;
						id_ = resultData.data.id;
						totalKoin_ = resultData.data.total_coin;
						game.state.start("Ingame");
					}
				}
			});
		}
	},
	DestroySubLvlScreen:function(){
		PlayUiClick();
		for(i=0;i<subTrashcan.length;i++)
		{
			subTrashcan[i].destroy();
		}
	},
	backToMainMenu: function(){
			PlayUiClick();
		game.state.start("MainMenu");

	}
}
