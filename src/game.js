/*global mainmenu*/
/*global ingame*/
/*global Phaser*/
/*global winScreen*/
var game = new Phaser.Game(1920, 1080, Phaser.CANVAS, 'game-canvas');
var soundGame = new Phaser.Game(0,0,Phaser.CANVAS);
var scaleMode = Phaser.ScaleManager.SHOW_ALL;
var totalLevel = conf_totalLevel;
var scoreUser = 0;
var timerUser = 0;
var isWin = true;
var gameLimit = 0;
var maxWidth = 1920;
var maxHeight = 900;
var currentLevel = 0;
var hintMinusCoin = 5;
var lastLevel = 0; //not last playing, tapi last level, level tertinggi yang dimaenin
var subTrashcan = []; //tempat sampah buat level select (array yang isinya semua element sublevel yang nantinya di destroy pas hide sublevel);
var gotoSubSelectLevel = false; //overide state level select untuk langsung manggil screen sublevel
var key_;
var id_;
var totalKoin_ = 0;
var difKoin_;
var isGuest = false;
var isLogin = false;
var isOneCycle = false;
var isMobile = false; //initiate as false
var HistoryAddCoin = [];
var isIngame = false;
var overlayBg;
var coinIsInit = false;

// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

WebFontConfig = {
  //  'active' means all requested fonts have finished loading
  //  We set a 1 second delay before calling 'createText'.
  //  For some reason if we don't the browser cannot render the text the first time it's created.
  active: function() { game.time.events.add(Phaser.Timer.SECOND, loadGame, this); },
  //  The Google Fonts we want to load (specify as many as you like in the array)
  google: {families :[ 'Lato:300,400:latin']},  // NEW UPDATE PIXELNINE
};
function loadGame(){};

if(isMobile){
  if(window.innerWidth < window.innerHeight){
    alert("Kalian harus memainkan game ini dengan keadaan landscape, window akan di refresh 3 detik kemudian");
    setTimeout(function(){
      location.reload();
    }, 3000);
  }
  $( window ).resize(function() {
    cssLoad();
    if(window.innerWidth < window.innerHeight)
    {
      alert("Kalian harus memainkan game ini dengan keadaan landscape, window akan di refresh 3 detik kemudian");
      setTimeout(function(){
        location.reload();
      }, 3000);
    }
  });
}

soundGame.state.add("SoundLoader",soundLoader);
game.state.add("LevelGenerator",levelGenerator);
game.state.add("LevelSelect",LevelSelect);
game.state.add("MainMenu",mainmenu);
game.state.add("Ingame",ingame);
game.state.add("ResultScreen",winScreen);
game.state.add("Boot",bootState);
game.state.start("Boot");
//soundGame.state.start("SoundLoader");
