var isFbLogin = false;
var fbId;
window.fbAsyncInit = function() {
  FB.init({
    appId      : '166672083743530',
    xfbml      : true,
    version    : 'v2.7'
  });
};

(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function ShareFb(){
  if(!isFbLogin){
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
          ShareLevel();
      }
      else {
        FB.login(function(response) {
          if (response.status === 'connected') {
              ShareLevel();
          }
        });
      }
    });
  }
}

function ShareLevel(){
  var shareLevels = 0;
  if(currentLevel%15 == 0)
    shareLevels = 15;
  else
    shareLevels = currentLevel%15

  console.log("share level is "+shareLevels);

  FB.ui({
    method: 'feed',
    link: 'https://TandaFuntastrip.com/games/cariin_dong/index.php?level='+currentLevel,
    caption: 'AKU TELAH BERHASIL MEMENANGKAN LEVEL '+shareLevels+' DI GAME CARIIN DONG!',
    name : "Cariin Dong! Game from Tanda Funtastrip",
    picture:"https://www.tandafuntastrip.com/games/cariin_dong_staging/Share/"+currentLevel+".jpg",
  }, function(response){
    if(response.post_id != null)
    {
      AddCoinFromShareLevel();
    }
  });
}

function ShareGames(){
  console.log("SHARE GAMES");
  FB.ui({
    method: 'feed',
    link: 'https://TandaFuntastrip.com/games/cariin_dong/index.php',
    caption: 'AYO BERMAIN GAME CARIIN DONG',
    name : "Cariin Dong! Game from Tanda Funtastrip",
    picture:"https://www.tandafuntastrip.com/games/cariin_dong_staging/asset/SplashScreen/background splash.jpg",
  }, function(response){
    if(response.post_id != null)
    {
      AddCoinFromShareGame();
    }
  });
}
