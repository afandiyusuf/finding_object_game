var gulp = require('gulp'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps')

var sourceJs = [
  "src/socialActivity.js",
	"src/globalFunction.js",
  "src/objectDataAll.js",
	"src/lvl.js",
	"src/state/mainmenu.js",
	"src/state/soundLoader.js",
	"src/state/levelSelect.js",
	"src/state/ingame.js",
	"src/state/winscreen.js",
	"src/state/boot.js",
	"src/state/levelGenerator.js",
	"src/game.js",
	"src/main.js",
	"src/horizontalSlider.js"
]

gulp.task('build-js',function(){
  return gulp.src(sourceJs)
    .pipe(sourcemaps.init())
    .pipe(concat('src.min.js'))
    .pipe(gutil.env.type == "production" ? uglify() : gutil.noop())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('src/min'))
})

gulp.task('default',['build-js']);
gulp.task('watch',function(){
  gulp.watch('src/**/*.js', ['build-js']);
});
